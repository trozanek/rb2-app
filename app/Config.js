Ext.define('RB2.Config', {
	singleton: true,
	config: {
		env: 'prod',

		environments: [
            { name: 'dev', url: 'http://192.168.1.108/rababear/' },
            { name: 'test', url: 'http://rababear.pl:8011/' },
            { name: 'prod', url: 'http://rababear.pl/' }
        ]

	},

    getEnv: function() {
        for (var i = 0; i< this.config.environments.length; i++) {
            if (this.config.environments[i].name === this.config.env) {
                return { name: this.config.env, url: this.config.environments[i].url };
            }
        }
    },

    getServicesUrl: function() {
    	var services;
		switch (this.getEnv().name) {
			default:  
				services = {
					partners: 'API2/api.php?a=partners',
					user: 'API2/api.php?a=user',
					categories: 'API2/api.php?a=categories',
					feed: 'API2/api.php?a=wall',
					trans: 'API2/api.php?a=trans',
					menu: 'API2/api.php?a=menu',
					checkin: 'API2/api.php?a=checkin',
					reviews: 'API2/api.php?a=reviews',
					ranking: 'API2/api.php?a=ranking',
					qrcode: 'API2/api.php?a=qrcode',
					register: 'API2/api.php?a=register'
				}
				break;
		}
		return services;
    }
})