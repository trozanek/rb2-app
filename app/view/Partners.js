Ext.define('RB2.view.Partners', {
	extend: 'Ext.Container',
	xtype: 'partners',
	requires: [
		'RB2.view.partners.YourPartners',
		'RB2.view.partners.PartnersCity',
		'RB2.view.partners.PartnersNearby',
		'RB2.view.partners.PartnersSearch'
	],
	config: {
		layout: 'fit',
		items: [
			{
				xtype: 'maintitlebarpartners'
			}, {
				xtype: 'carousel',
				cls: 'carousel',
				activeItem: 2,
				indicator: false,
				items: [
					{
						xtype: 'container',
						layout: 'fit',
						cls: 'partners-search',
						itemId: 'partners-search',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: RB2.util.i18n.translate('Search')
							}, {
								xtype: 'partnerssearch'
							}
						]
						// hidden: true
						
					},
					{
						xtype: 'container',
						layout: 'fit',
						cls: 'your-partners',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: RB2.util.i18n.translate('Favorites')
							}, {
								xtype: 'yourpartners'
							}
						]
						
					}, {
						xtype: 'container',
						layout: 'fit',
						cls: 'partners-categories',
						items: [
							{
								xtype: 'partnerscategorieswrapper'
							}
						]
					}, {
						xtype: 'container',
						layout: 'fit',
						cls: 'partners-nearby',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: RB2.util.i18n.translate('Nearby'),
								itemId: 'nearbyTitlebar',
								items: [{
									xtype: 'button',
									iconCls: 'locked',
									cls: 'aligned-right',
									style: 'margin-right: 1.5em; font-size: 1.3em;',
									iconMask: true,
									align: 'right',
									action: 'lockmap'
								}]
							}, {
								xtype: 'partnersnearby'
							}, {
								xtype: 'container',
								name: 'nearbyinfo',
								html: RB2.util.i18n.translate('longpressMap'),
								height: 'auto',
								bottom: '1.5em',
								width: '100%',
								style: 'background: rgba(0,0,0,.75); color: #FFF; padding: 1em 2em 1em 1em; text-align: center; margin-left: 5%; margin-right: 10%; font-size: .75em;'
							}
						]
					}, {
						xtype: 'container',
						layout: 'fit',
						cls: 'partners-city',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: RB2.util.i18n.translate('City'),
								itemId: 'cityTitlebar',
								items: [{
									xtype: 'button',
									iconCls: 'globe',
									cls: 'aligned-right',
									style: 'margin-right: 1.5em; font-size: 1.3em;',
									iconMask: true,
									align: 'right',
									action: 'changecity',
									text: RB2.util.i18n.translate('ChangeCity')
								}]
							}, {
								xtype: 'partnerscity'
							}
						]
					}
				]
			}

		],
		listeners: {
			initialize: function() {
				
		        var carousel = this.down('carousel')
				carousel.on('activeitemchange', function() {
					console.log('change');
					carousel.getActiveItem().getItems().each(function(item) {
						item.fireEvent('chosen');
					});
				});
			},
			show: function() {
				
			}
		}
	}
});