Ext.define('RB2.view.Partner', {
	extend: 'Ext.Container',
	xtype: 'partner',
	requires: [
		'RB2.view.partner.Home',
		'RB2.view.partner.Promos',
		'RB2.view.partner.Info',
		'RB2.view.partner.Lokale',
		'RB2.view.partner.Discounts',
		'RB2.view.partner.Rewards',
		'RB2.view.partner.Offer',
		'RB2.view.partner.PartnerMap',
		'RB2.view.partner.Transactions',
		'RB2.view.partner.Reviews',
		'RB2.view.partner.Ranking'
	],
	config: {
		layout: 'fit',
		cls: 'partner-view',
		itemId: 'partnerView',
		items: [
			{
				xtype: 'partnermap'
			},
			{
				xtype: 'maintitlebarpartners'
			},
			{
				xtype: 'container',
				cls: 'partner-wrapper',
				itemId: 'partnerWrapper',
				layout: 'fit',
				items: [
					{
						xtype: 'container',
						docked: 'top',
						itemId: 'partnerDetailsLogo',
						cls: 'partner-head',
						tpl: new Ext.XTemplate(
							'<div class="tile"><img src="{logo}"/></div>',
							'<div class="desc">',
								'<h2>{name}</h2>',
								'<tpl if="this.multipleLocations(lokale)">',
									'<h3>{lokale:this.multipleLocations}</h3>',
								'<tpl else>',
									'<tpl for="lokale">',
										'<h3>{ulica} {nr}, {cityName}</h3>',
									'</tpl>',
								'</tpl>',
								// '<tpl if="fav == true">fav</tpl>',
								// '<tpl if="fav === false">not fav</tpl>',
							'</div>',
							{
								multipleLocations: function(lokale) {
									if (lokale.length > 1) {
										var result = lokale[0].ulica +' '+lokale[0].nr+', '+lokale[0].cityName+' i '+ (lokale.length-1) +' innych';
										return result;
									} else {
										return false;
									}
								}
							}
						)
					},
					{
						xtype: 'carousel',
						cls: 'carousel',
						defaults: {
							cls: 'partner-section'
						},
						indicator: false,
						items: [
							{
								xtype: 'container',
								layout: 'fit',
								items: [
									{
										xtype: 'titlebar',
										docked: 'top',
										title: RB2.util.i18n.translate('Partner')
									}, {
										xtype: 'partnerhome'
									}
								]
								
							},
							{
								xtype: 'container',
								layout: 'fit',
								items: [
									{
										xtype: 'titlebar',
										docked: 'top',
										title: RB2.util.i18n.translate('Info')
									}, {
										xtype: 'partnerinfo'
									}
								]
							},
							{
								xtype: 'container',
								layout: 'fit',
								items: [
									{
										xtype: 'titlebar',
										docked: 'top',
										title: RB2.util.i18n.translate('Discounts')
									}, {
										xtype: 'partnerdiscounts'
									}
								]
							},
							{
								xtype: 'container',
								layout: 'fit',
								items: [
									{
										xtype: 'titlebar',
										docked: 'top',
										title: RB2.util.i18n.translate('Venues')
									}, {
										xtype: 'partnerlokale'
									}
								]
								
							},
							{
								xtype: 'container',
								layout: 'fit',
								items: [
									{
										xtype: 'titlebar',
										docked: 'top',
										title: RB2.util.i18n.translate('Offer')
									}, {
										xtype: 'partneroffer'
									}
								]
							},
							{
								xtype: 'container',
								layout: 'fit',
								items: [
									{
										xtype: 'titlebar',
										docked: 'top',
										title: RB2.util.i18n.translate('Promo')
									}, {
										xtype: 'partnerpromos'
									}
								]
								
							},
							{
								xtype: 'container',
								layout: 'fit',
								items: [
									{
										xtype: 'titlebar',
										docked: 'top',
										title: RB2.util.i18n.translate('TransHistory')
									}, {
										xtype: 'partnertransactions'
									}
								]
							},
							{
								xtype: 'container',
								layout: 'fit',
								items: [
									{
										xtype: 'titlebar',
										docked: 'top',
										title: RB2.util.i18n.translate('Reviews')
									}, {
										xtype: 'partnerreviews'
									}
								]
							},
							{
								xtype: 'container',
								layout: 'fit',
								items: [
									{
										xtype: 'titlebar',
										docked: 'top',
										title: RB2.util.i18n.translate('PartnerRank')
									}, {
										xtype: 'partnerranking'
									}
								]
								
							},
							{
								xtype: 'container',
								layout: 'fit',
								items: [
									{
										xtype: 'titlebar',
										docked: 'top',
										title: RB2.util.i18n.translate('Rewards')
									}, {
										xtype: 'partnerrewards'
									}
								]
							},
						]
					},
					{
						xtype: 'button',
						action: 'showqrformnotoggle',
						cls: 'qrcode-bottom'
					}
				]
			}
		],
		listeners: {
			show: function() {
				
				this.down('maintitlebarpartners').down('button[action=showpartners]').show();
				this.down('maintitlebarpartners').down('button[action=toggleleftmenu]').hide();
				this.down('maintitlebarpartners').down('button[action=showpartnermenu]').show();
				this.down('maintitlebarpartners').down('button[action=showmap]').show();
				this.down('maintitlebarpartners').down('button[action=refreshpartners]').hide();
				this.down('maintitlebarpartners').setTitle(this.getData().name);
				

				this.down('#partnerDetailsLogo').setData(this.getData());
				this.down('partnerhome').setData(this.getData());
				this.down('partnerpromos').setData(this.getData());
				this.down('partnerinfo').setData(this.getData());
				Ext.getStore('Lokale').setData(this.getData().lokale);
				this.down('partnerdiscounts').setData(this.getData());
				this.down('partnertransactions').setData(this.getData());
				this.down('partnerreviews').setData(this.getData());
				Ext.getStore('Rewards').setData(this.getData().nagrody);
				Ext.getStore('Offer').removeAll();
				Ext.getStore('Offer').load({
					params: { partner: this.getData().id }
				});
				Ext.getStore('Ranking').removeAll();
				Ext.getStore('Ranking').load({
					params: { partner: this.getData().id }
				});

				this.down('carousel').setActiveItem(0);

			},
			hide: function() {
				this.down('maintitlebarpartners').down('button[action=showpartners]').hide();
				this.down('maintitlebarpartners').down('button[action=showpartnermenu]').hide();
				this.down('maintitlebarpartners').down('button[action=showmap]').hide();
				this.down('maintitlebarpartners').down('button[action=refreshpartners]').show();
			},
			initialize: function() {
				var carousel = this.down('carousel')
				carousel.on('activeitemchange', function() {
					console.log('change');
					console.log(carousel.getActiveItem())
					carousel.getActiveItem().getItems().each(function(item) {
						item.fireEvent('chosen');
					});
				})
			}
		}
	}
});