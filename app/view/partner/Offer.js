Ext.define('RB2.view.partner.Offer', {
	extend: 'Ext.List',
	xtype: 'partneroffer',

	config: {
		grouped: true,
		autoDestroy: true,
		store: {
			xtype: 'offerstore',
			autoLoad: false
		},
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		disableSelection: true,
		cls: 'partners-list no-disclosure offers-list',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="desc"><h2>{name}</h2><tpl for="prices"><h3>{variant} - {price} {parent.currency}</h3></tpl></div></tpl>'),

		listeners: {
			painted: function() {
				this.getStore().setGrouper({
					groupFn: function(record) {
		                return record.get('cat');
		            },
		            sortProperty: 'cat'
				})
			}
		}
	}
});