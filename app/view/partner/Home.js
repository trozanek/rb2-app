Ext.define('RB2.view.partner.Home', {
	extend: 'Ext.Container',
	xtype: 'partnerhome',
	
	config: {
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		cls: 'partner-home',
		items: [
			{
				xtype: 'button',
				text: RB2.util.i18n.translate('Redeem'),
				name: 'redeemBtn',
				action: 'redeem',
				cls: 'redeem-btn',
				iconCls: 'maps'
			},
			{
				xtype: 'container',
				name: 'transInfoBox',
				html: '',
				hidden: true
			},
			{
				xtype: 'container',
				layout: 'hbox',
				items: [
					{
						xtype: 'container',
						itemId: 'yourLevel',
						cls: 'exp-info lvl',
						html: '<span>'+RB2.util.i18n.translate('YourLevel')+'<span>',
						flex: 1
					},
					{
						xtype: 'container',
						itemId: 'yourDiscount',
						cls: 'exp-info disc',
						html: '<span>'+RB2.util.i18n.translate('YourLevel')+'<span>',
						flex: 1
					}
				]
			},
			{
				xtype: 'container',
				itemId: 'yourAdvance',
				cls: 'advance',
				html: '<span>'+RB2.util.i18n.translate('YourAdvance')+'</span><div class="progress-bar"><div class="progress-bar-inner"></div></div><span class="prev-lvl">0 lvl</span><span class="next-lvl">1 lvl</span>'
			}
		],
		listeners: {
			painted: function() {
				var partnerId = this.getData().id,
					store = Ext.getStore('Trans'),
					self = this;
					
				// self.showRedeemBtn();
				// store.each(function(record) {
				// 	data = record.data;
				// 	if (data.partner === partnerId && data.status === 0) {
				// 		self.hideRedeemBtn(record.data);
				// 	} 
				// })

				this.down('#yourLevel').setHtml('<span>T'+RB2.util.i18n.translate('YourLevel')+'</span><p>' + this.getData().level + '</p>');
				if (this.getData().discount_sign === '%') {
					this.down('#yourDiscount').setHtml('<span>'+RB2.util.i18n.translate('YourDiscount')+'</span><p> ' + this.getData().discount + ' ' + this.getData().discount_sign + '</p>');
				} else {
					this.down('#yourDiscount').setHtml('<span>'+RB2.util.i18n.translate('YourDiscount')+'</span><p>' + this.getData().discount + ' <span style="font-size: .4em; position: relative; top: -1.8em;">' + this.getData().discount_sign + '</span></p>');
				}
				this.down('#yourAdvance').setHtml('<span>'+RB2.util.i18n.translate('YourAdvance')+'</span><div class="progress-bar"><div class="inner" style="width: '+this.getData().advance+'%"></div></div><span class="prev-lvl lvl-mrk">' + this.getData().level + ' lvl</span><span class="next-lvl lvl-mrk">' + this.getData().nextLevel +' lvl</span>');
				console.log(this.getData().qr_only)
				// if (this.getData().qr_only === 1) {
				// 	this.down('[name=redeemBtn]').hide();
				// }
			}
		}
	},
	showRedeemBtn: function() {
		this.down('[name=transInfoBox]').hide();
		this.down('[name=redeemBtn]').show();
	},
	hideRedeemBtn: function(record) {
		this.down('[name=transInfoBox]').show();
		this.down('[name=redeemBtn]').hide();
		this.displayTransInfo(record);
	},
	displayTransInfo: function(record) {
		console.log(record);
		var transData = record;
		console.log('sadasd');
		this.down('[name=transInfoBox]').setHtml('<div class="infoBox"><div class="inner"><p>Twoja transakcja oczekuje na realizację</p><p>Nr transakcji: #'+transData.id+'. PIN: '+transData.pin+'</p><p>Pozostało '+transData.remainingTime+' na realizację.</div></div>')
	}
});