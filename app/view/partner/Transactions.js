Ext.define('RB2.view.partner.Transactions', {
	extend: 'Ext.List',
	xtype: 'partnertransactions',

	config: {
		store: {
			xtype: 'transstore',
			sorters: [
				{
					property: 'status',
					direction: 'ASC'
				}
			]
		},
		emptyText: RB2.util.i18n.translate('EmptyTrans'),
		disableSelection: true,
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		cls: 'partners-list no-disclosure transactions-partner',
		itemTpl: new Ext.XTemplate(
			'<tpl if="status == 0">',
				'<tpl for="."><div class="trans-body"><div class="tile"><img src="{partnerLogo}"/></div><div class="desc"><h2>{partnerNazwa}</h2><h3>ID: #{id}</h3><h3>PIN: {pin}</h3><h3>Pozostały czas: {remainingTime}</h3></div></div><div class="trans-status">{statusName}</div></tpl>',
			'</tpl>',
			'<tpl if="status == 1">',
				'<tpl for="."><div class="trans-body"><div class="tile"><img src="{partnerLogo}"/></div><div class="desc"><h2>{partnerNazwa}</h2><h3>ID: #{id}</h3><h3>PIN: {pin}</h3></div></div><div class="trans-status">{statusName}</div></tpl>',
			'</tpl>',
			'<tpl if="status == 2">',
				'<tpl for="."><div class="trans-body"><div class="tile"><img src="{partnerLogo}"/></div><div class="desc"><h2>{partnerNazwa}</h2><h3>ID: #{id}</h3><h3>Wydałeś: {kwota}</h3><h3>Otrzymałeś {znizka} {znizka_sign} zniżki</h3></div></div><div class="trans-status">{statusName}</div></tpl>',
			'</tpl>',
			'<tpl if="status == 4">',
				'<tpl for="."><div class="trans-body"><div class="tile"><img src="{partnerLogo}"/></div><div class="desc"><h2>{partnerNazwa}</h2><h3>ID: #{id}</h3><h3>Przekroczenie czasu</h3></div></div><div class="trans-status">{statusName}</div></tpl>',
			'</tpl>'),


		listeners: {
			chosen: function(){
				console.log('filtering');
				this.getStore().clearFilter();
				this.getStore().filter([
					{
						property: 'partner',
						value: this.getData().id.toString()
					}
				]);
			},
			painted: function() {
				this.fireEvent('chosen');
			}
		}
	}
});