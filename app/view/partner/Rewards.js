Ext.define('RB2.view.partner.Rewards', {
	extend: 'Ext.List',
	xtype: 'partnerrewards',

	config: {
		store: 'Rewards',
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		disableSelection: true,
		emptyText: 'Partner Nie zdefiniował żadnych nagród',
		itemTpl: new Ext.XTemplate('{name}'),
		listeners: {
			
		}
	}
});