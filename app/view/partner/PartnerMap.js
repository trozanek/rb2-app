Ext.define('RB2.view.partner.PartnerMap', {
    extend: 'Ext.Map',
	xtype: 'partnermap',
    config: {
    	docked: 'top',
        autoDestroy: true,
    	itemId: 'partnerMap',
    	style: 'top: 2.6em; bottom: 20%; left: 0; right: 0; position: absolute !important;',
    	cls: 'partner-map',
    	layout: 'fit',
        mapOptions:{
            zoom: 16,
            disableDefaultUI: true,
        //--Available Map Options--//

            panControl: true,
            zoomControl: true,
            draggable: true,
            mapTypeControl: false,
            scaleControl: true,
            streetViewControl: true,
            overviewMapControl: false
        },
        listeners: {
            painted: function() {
                console.log('painted');
                this.addMarkers();  
            },
            maprender: function() {
                console.log('render');
                this.addMarkers();
            }
        }
    },
    addMarkers: function() {

        var lokale = this.up('partner').getData().lokale
            partner = this.up('partner').getData(),
            self = this;
            
        
        if (Ext.ComponentQuery.query('partnermap')[0].getMap() !== null) {
            if (lokale || lokale!==undefined) {
                var lat, lon, locations = new Array, closest;
                for ( var i=0; i<lokale.length;i++) {
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lokale[i].lat, lokale[i].lon),
                        map: Ext.ComponentQuery.query('partnermap')[0].getMap(),
                        icon: new google.maps.MarkerImage(partner.logo, null, null ,null ,new google.maps.Size(35,35))

                    });
                    lat = lokale[i].lat;
                    lon = lokale[i].lon;

                    var p1 = new google.maps.LatLng(lat, lon),
                        p2;

                    if (RB2.userLat && RB2.userLon) {
                        p2  = new google.maps.LatLng(RB2.userLat, RB2.userLon);
                    } else {
                        p2 = new google.maps.LatLng(52.411634, 16.925812);
                    }

                    var loc = {
                        lat : lat,
                        lon : lon,
                        distance : RB2.util.MapsTools.distance(p1,p2)
                    }
                    locations.push(loc);
                }
                console.log(locations);
                closest = RB2.util.MapsTools.minMaxDistance(locations).min;

                // console.log(locations);
                // console.log(RB2.util.MapsTools.minMaxDistance(locations));

                self.setMapCenter({ latitude: closest.lat, longitude: closest.lon});
                
            }
        }
    }
})