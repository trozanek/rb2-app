Ext.define('RB2.view.partner.Info', {
	extend: 'Ext.Container',
	xtype: 'partnerinfo',
	
	config: {
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		cls: 'content-box',
		tpl: new Ext.XTemplate('{desc}')
	}
});