Ext.define('RB2.view.partner.Ranking', {
	extend: 'Ext.List',
	xtype: 'partnerranking',

	config: {
		store: {
			xtype: 'rankingstore'
		},
		emptyText: RB2.util.i18n.translate('EmptyRank'),
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		disableSelection: true,
		cls: 'partners-list no-disclosure rank',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile"><img src="{avatar}"/></div><div class="desc"><h2>{place}</h2><h3>{usrName}</h3></div></tpl>'),
		listeners: {
			painted: function(){
				this.fireEvent('chosen');
			},
			chosen: function() {
				this.getStore().clearFilter();
			}
		}
	}
});