Ext.define('RB2.view.partner.Header', {
	extend: 'Ext.Container',
	xtype: 'partnerheader',
	
	config: {
		tpl: new Ext.XTemplate('<div style="width: 100%; height: 4em; background: #FFF; background-image: url({logo}); background-size: auto 100%; background-position: 50% 50%; background-repeat: no-repeat; "><span>{name}</span></div>')
	}
});