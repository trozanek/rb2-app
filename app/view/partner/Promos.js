Ext.define('RB2.view.partner.Promos', {
	extend: 'Ext.List',
	xtype: 'partnerpromos',

	config: {
		store: {
			xtype: 'feedstore'
		},
		emptyText: RB2.util.i18n.translate('EmptyPromos'),
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		cls: 'partners-list feed',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile"><img src="'+RB2.Config.getEnv().url+'{avatar}"/></div><div class="desc"><h2>{head}</h2><h3>{content}</h3></div></tpl>'),


		listeners: {
			chosen: function(){
				console.log('filtering');
				this.getStore().clearFilter();
				this.getStore().filter([
					{
						property: 'partner',
						value: this.getData().id.toString()
					}, {
						property: 'scopePartner',
						value: true
					}
				]);
			},
			painted: function() {
				this.fireEvent('chosen');
			}
		}
	}
});