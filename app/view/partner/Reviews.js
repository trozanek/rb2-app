Ext.define('RB2.view.partner.Reviews', {
	extend: 'Ext.List',
	xtype: 'partnerreviews',

	config: {
		store: {
			xtype: 'reviewsstore'
		},
		emptyText: RB2.util.i18n.translate('EmptyReviews'),
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		disableSelection: true,
		cls: 'partners-list feed',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile"><img src="{avatar}"/></div><div class="desc"><h2>{usrName}</h2><h3>{content}</h3></div></tpl>'),
		
		listeners: {
			painted: function(){
				this.fireEvent('chosen');
				console.log('reviews painted');
			},
			chosen: function() {
				this.getStore().clearFilter();
				this.getStore().filter([
					{
						property: 'partner',
						value: this.getData().id
					}
				]);
			}
		}
	}
});