Ext.define('RB2.view.partner.Discounts', {
	extend: 'Ext.Container',
	xtype: 'partnerdiscounts',
	
	config: {
		cls: 'content-box',
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		tpl: new Ext.XTemplate('<p>{[this.translate("forStart")]}: {znizka_start} {discount_sign}</p><p>{[this.translate("Max")]}: {znizka_max} {discount_sign}</p>',
			'<tpl if="narastanie &gt; 1">',
				'<ul>',
					'<tpl for="znizki"><li><p><span>{[this.translate("forSpending")]} {kwota} {parent.currency} - </span><span>{znizka} {parent.discount_sign} {[this.translate("onNextVisit")]}</span></li></tpl>',
				'</ul>',
			'<tpl else>',
				'{[this.translate("forEvery")]} {kwotaWzrostu} {currency} - {[this.translate("onePercentMore")]}',
			'</tpl>',
			'<p>{[this.translate("pointsExpireAfter")]} {pointsExpire} {[this.translate("daysM")]}',
			{
				translate: function(value) {
					return RB2.util.i18n.translate(value);
				}
			}
			)
	}
});