Ext.define('RB2.view.partner.Lokale', {
	extend: 'Ext.List',
	xtype: 'partnerlokale',

	config: {
		store: {
			xtype: 'lokalestore',
			autoLoad: false
		},
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		cls: 'partners-list no-disclosure',
		itemTpl: new Ext.XTemplate('<div class="desc"><h2>{name}</h2><h3>{ulica} {nr}, {cityName}</h3><div class="show-local-map x-button-icon compass"></div></div>'),
		itemHeight: function() {
			var width = Ext.componentQuery.query('.tile')[0].width;
			return width;
		},
		listeners: {
			
		}
	}
});