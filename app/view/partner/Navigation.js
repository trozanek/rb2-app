Ext.define('RB2.view.partner.Navigation', {
	extend: 'Ext.Container',
	xtype: 'partnernavigation',
	
	config: {
		cls: 'nav nav-right nav-panel',
        itemId: 'right-menu',
        layout: 'fit',
        hidden: true,
        items: [
            {
                xtype: 'titlebar',
                docked: 'top',
                title: 'Partner'
            }, {
                xtype: 'container',
                scrollable: true,
                itemId: 'partnerMenuButtons',
                items: [
                	{
                		xtype: 'button',
                        cls: 'partner-nav-button',
                		text: RB2.util.i18n.translate('Partner'),
                		action: '0'
                	},
                    {
                        xtype: 'button',
                        text: RB2.util.i18n.translate('Info'),
                        cls: 'partner-nav-button',
                        action: '1'
                    },
                    {
                        xtype: 'button',
                        text: RB2.util.i18n.translate('Discounts'),
                        cls: 'partner-nav-button',
                        action: '2'
                    },
                    {
                        xtype: 'button',
                        text: RB2.util.i18n.translate('Venues'),
                        cls: 'partner-nav-button',
                        action: '3'
                    },
                    {
                        xtype: 'button',
                        text: RB2.util.i18n.translate('Offer'),
                        cls: 'partner-nav-button',
                        action: '4'
                    },
                	{
                		xtype: 'button',
                		text: RB2.util.i18n.translate('Promo'),
                        cls: 'partner-nav-button',
                		action: '5'
                	},
                	{
                		xtype: 'button',
                		text: RB2.util.i18n.translate('TransHistory'),
                        cls: 'partner-nav-button',
                		action: '6'
                	},
                	{
                		xtype: 'button',
                		text: RB2.util.i18n.translate('Reviews'),
                        cls: 'partner-nav-button',
                		action: '7'
                	},
                	{
                		xtype: 'button',
                		text: RB2.util.i18n.translate('PartnerRank'),
                        cls: 'partner-nav-button',
                		action: '8'
                	},
                    {
                        xtype: 'button',
                        text: RB2.util.i18n.translate('Rewards'),
                        cls: 'partner-nav-button',
                        action: '9'
                    },
                ]
            }
        ]
	}
});