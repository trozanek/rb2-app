Ext.define('RB2.view.Register', {
	extend: 'Ext.form.Panel',
	xtype: 'register',
	config: {
		layout: 'fit',
		style: 'z-index: 50;',
		cls: 'login-screen',
		hidden: true,
		items: [
			{
				xtype: 'fieldset',
				cls: 'bottom-login',
				style: 'padding: 5%;',
				items: [
					{
						xtype: 'textfield',
						name: 'registerlogin',
						placeHolder: RB2.util.i18n.translate('Nick')
					}, 
					{
						xtype: 'textfield',
						name: 'registermail',
						placeHolder: RB2.util.i18n.translate('email')
					},
					{
						xtype: 'passwordfield',
						name: 'registerpass',
						placeHolder: RB2.util.i18n.translate('password')
					}, 
					{
						xtype: 'passwordfield',
						name: 'registerpass_ver',
						placeHolder: RB2.util.i18n.translate('passwordConfirm')
					},
					{
						xtype: 'button',
						text: RB2.util.i18n.translate('register'),
						action: 'goRegister',
						ui: 'action'
					}
				]
			},
			{
				xtype: 'fieldset',
				docked: 'bottom',
				items: [
					{
						xtype: 'button',
						text: RB2.util.i18n.translate('BackToLogin'),
						action: 'hideregisterform',
						style: 'margin: 5%'
					}
				]
			}
			
		]
	}
})