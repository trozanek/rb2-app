Ext.define('RB2.view.QRConfirm', {
	extend: 'Ext.Panel',
	xtype: 'qrconfirm',
	config: {
		centered: true,
		width: '90%',
		height: '90%',
		layout: 'fit',
		style: 'z-index: 3; background; #FFF',
		hidden: true,
		scrollable: true,
		items: {
			xtype: 'fieldset',
			items: [{
					xtype: 'container',
					id: 'codeContainer',
					html: '',
					cls: 'qrConfirm',
					docked: 'top'

				},{
					xtype: 'button',
					action: 'closeconfirm',
					text: RB2.util.i18n.translate('Close'),
					docked: 'bottom'
				}
			]
		},
		listeners: {
			show: function() {
				
			},
			codeScanned: function(codeKey, amt, ts) {
				var self = this;
				console.log('codeId ' + codeKey);
				console.log('amt ' + amt);
				Ext.Viewport.setMasked({xtype: 'loadmask', message: RB2.util.i18n.translate('Moment')});
				Ext.Ajax.request({
				   url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().qrcode,
				   method: 'GET',
				   disableCaching: true,
				   useDefaultXhrHeader: false,
				   params: {
						'usr': window.localStorage.getItem('RB2-current-usr'),
						'kwota': amt,
						'qr': codeKey,
						'ts': ts
				   },
				   success: function(response, opts) {
				   		var localmd5 = window.localStorage.getItem('RB2-partners-md5'),
				   			resultSet = Ext.JSON.decode(response.responseText),
				   			records = resultSet.result;
				   			if (resultSet.success) {
				   				self.down('#codeContainer').setHtml('<h5 style="line-height: 1em; border-bottom: 1px solid #CCC; padding-bottom: .5em;">'+records[0].nazwa_marki+'<h5>'+RB2.util.i18n.translate('Discount')+'</h5><p>'+records[0].discount+'</p><h5>'+RB2.util.i18n.translate('ToPay')+'</span><p>'+records[0].to_pay+' PLN</p>');
				   				var pIdx = Ext.getStore('Partners').findExact('id',records[0].pId),
				   					pRecord = Ext.getStore('Partners').getAt(pIdx);
				   				
				   				pRecord.set('fav',true);

				   				console.warn('pIdx', pRecord);
				   			} else {
				   				Ext.Msg.alert('Błąd', resultSet.msg, Ext.emptyFn);
				   				self.hide();
				   			} 
				   			console.log(resultSet);
				   			Ext.Viewport.setMasked(false);
				   			RB2.util.LoadPartners.load();

				   			
				   		
				   },
				   failure: function(response, opts) {
				      console.log('server-side failure with status code ' + response.status);
				      Ext.Msg.alert(RB2.util.i18n.translate('PoorConnection'), RB2.util.i18n.translate('PoorConnectionInfo'), Ext.emptyFn);
				      Ext.Viewport.setMasked(false);
				   }
				});
			}
		}
	}
});