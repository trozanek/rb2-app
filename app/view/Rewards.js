Ext.define('RB2.view.Rewards', {
	extend: 'Ext.Container',
	xtype: 'rewards',

	config: {
		layout: 'fit',
		items: [
			{
				xtype: 'maintitlebarrewards'
			}, {
				xtype: 'carousel',
				cls: 'carousel',
				items: [
					{
						xtype: 'container',
						layout: 'fit',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: 'Do odebrania'
							}, {
								html: 'Do odebrania'
							}
						]
					},
					{
						xtype: 'container',
						layout: 'fit',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: 'Odebrane'
							}, {
								html: 'Odebrane'
							}
						]
						
					}, {
						xtype: 'container',
						layout: 'fit',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: 'W Twoim zasięgu'
							}, {
								html: 'W Twoim zasięgu'
							}
						]
					}
				]
			}

		],
		listeners: {
			initialize: function() {
				
			},
			show: function() {
				
			}
		}
	}
});