Ext.define('RB2.view.checkWithQR', {
	extend: 'Ext.Panel',
	xtype: 'checkwithqr',
	config: {
		centered: true,
		width: '90%',
		height: '90%',
		layout: 'fit',
		style: 'z-index: 3; background; #FFF',
		hidden: true,
		scrollable: null,
		items: {
			xtype: 'fieldset',
			items: [
				{
					xtype: 'numberfield',
					name: 'amount',
					placeHolder: RB2.util.i18n.translate('AmountSpent'),
					docked: 'top'
				}, {
					xtype: 'container',
					itemId: 'amountConfirmBox',
					layout: 'fit',
					cls: 'amountConfirm',
					html: RB2.util.i18n.translate('QRInstructions')
				}, {
					xtype: 'button',
					docked: 'bottom',
					text: RB2.util.i18n.translate('Cancel'),
					ui: 'decline',
					action: 'closeform'
				}, {
					xtype: 'button',
					docked: 'bottom',
					text: RB2.util.i18n.translate('Scan'),
					ui: 'confirm',
					action: 'openscanner'
				}
			]
		}
	}
});