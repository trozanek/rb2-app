Ext.define('RB2.view.Feed', {
	extend: 'Ext.Container',
	xtype: 'feed',

	config: {
		layout: 'fit',
		items: [
			{
				xtype: 'maintitlebarfeed'
			}, {
				xtype: 'carousel',
				cls: 'carousel',
				items: [
					{
						xtype: 'container',
						layout: 'fit',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: RB2.util.i18n.translate('AllFeed')
							}, {
								xtype: 'feedall'
							}
						]
					},
					{
						xtype: 'container',
						layout: 'fit',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: RB2.util.i18n.translate('FriendsOnly')
							}, {
								xtype: 'feedfriends'
							}
						]
						
					}, {
						xtype: 'container',
						layout: 'fit',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: RB2.util.i18n.translate('PartnersOnly')
							}, {
								xtype: 'feedpartners'
							}
						]
					}
				]
			}

		],
		listeners: {
			initialize: function() {
				
			},
			show: function() {
				
			}
		}
	}
});