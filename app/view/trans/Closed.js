Ext.define('RB2.view.trans.Closed', {
	extend: 'Ext.List',
	xtype: 'transclosed',
	requires: ['RB2.store.Trans'],
	config: {
		store: {
			xtype: 'transstore',
			filters: [
				{
					property: 'status',
					value: 2
				}
			]
		},
		emptyText: RB2.util.i18n.translate('EmptyTrans'),
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		cls: 'partners-list',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile"><img src="{partnerLogo}"/></div><div class="desc"><h2>{partnerNazwa}</h2><h3>ID: #{id}</h3><h3>{[this.translate("YouSpent")]}: {kwota}</h3><h3>{[this.translate("YouGot")]} {znizka} {znizka_sign} {[this.translate("discountD")]}</h3></div></tpl>',
			{
			translate: function(value) {
				return RB2.util.i18n.translate(value);
			}	
		})
		
		// listeners: {
		// 	painted: function() {
		// 		console.log('Store load');
		// 		this.getStore().load();
		// 	}
		// }
	}
});