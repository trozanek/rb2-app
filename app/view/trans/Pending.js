Ext.define('RB2.view.trans.Pending', {
	extend: 'Ext.List',
	xtype: 'transpending',
	requires: ['RB2.store.Trans'],
	config: {
		store: {
			xtype: 'transstore',
			filters: [
				{
					property: 'status',
					value: 1
				}
			]
		},
		emptyText: RB2.util.i18n.translate('EmptyTrans'),
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		cls: 'partners-list',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile"><img src="{partnerLogo}"/></div><div class="desc"><h2>{partnerNazwa}</h2><h3>ID: #{id}</h3><h3>PIN: {pin}</h3></div></tpl>')

		// itemHeight: function() {
		// 	return 'auto !important'
		// },
		// listeners: {
		// 	painted: function() {
		// 		console.log('Store load');
		// 		this.getStore().load();
		// 	}
		// }
	}
});