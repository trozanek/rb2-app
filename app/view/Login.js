Ext.define('RB2.view.Login', {
	extend: 'Ext.form.Panel',
	xtype: 'login',
	config: {
		layout: 'fit',
		style: 'z-index: 3;',
		cls: 'login-screen',
		hidden: true,
		scrollable: null,
		items: {
			xtype: 'fieldset',
			layout: 'vbox',
			items: [
				{
					xtype: 'container',
					flex: 1,
					cls: 'top-login'			
				},
				{
					xtype: 'container',
					cls: 'bottom-login',
					flex: 3,
					items: [
						{
							xtype: 'selectfield',
							name: 'country',
							placeHolder: 'Country/Kraj',
							options: [
								{
									text: 'United Kingdom',
									value: 'UK'
								},
								{
									text: 'Polska',
									value: 'PL'
								}
							]
						},
						{
							xtype: 'textfield',
							name: 'login',
							placeHolder: RB2.util.i18n.translate('email')
						}, {
							xtype: 'passwordfield',
							name: 'password',
							placeHolder: RB2.util.i18n.translate('password')
						}, {
							xtype: 'button',
							text: RB2.util.i18n.translate('logIn'),
							ui: 'action',
							action: 'logmein'
						}, {
							xtype: 'button',
							docked: 'bottom',
							text: RB2.util.i18n.translate('register'),
							action: 'showregisterform'
						}
					]
				}
				
			]
		},
		listeners: {
			initialize: function() {
				var self = this;
				Ext.data.JsonP.request({
					url: 'http://api.ipinfodb.com/v3/ip-country/?key=49005e4c9e3ceadf640a68032390364fc6aa3a7bb130106771e15831a932186d&format=json',
					callbackKey: 'callback',
					success: function(result) {
						self.down('[name=country]').setValue(result.countryCode);
					}
				})
				
			}
		}
	}
})