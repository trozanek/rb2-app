Ext.define('RB2.view.Main', {
    extend: 'Ext.Container',
    xtype: 'main',
    requires: [
        'Ext.TitleBar'
    ],
    config: {
        layout: 'fit',
        items: [
            {
                xtype: 'container',
                layout: 'card',
                cls: 'naved',
                itemId: 'navedcontainer',
                items: [
                    {
                        xtype: 'partners'
                    },
                    {
                        xtype: 'feed'
                    },
                    {
                        xtype: 'transactions'
                    },
                    {
                        xtype: 'rewards'
                    },
                    {
                        xtype: 'partner'
                    }
                ]
            }, {
                xtype: 'mainnavigation'
            }, {
                xtype: 'partnernavigation'
            }, {
                xtype: 'login'
            }, {
                xtype: 'register'
            }, {
                xtype: 'checkwithqr'
            }, {
                xtype: 'qrconfirm'
            }
        ],
        listeners: {
            initialize: function() {
                var navedcontainer = this.down('#navedcontainer');
                navedcontainer.on('activeitemchange', function() {
                
                    
                    navedcontainer.getActiveItem().getActiveItem().getItems().items[1].getActiveItem().getItems().each(function(item){
                        item.fireEvent('chosen');
                    });
                });
            }
        }
    }, 
    initialize: function() {
        RB2.util.CurrentUser.isLoggedRedirect();
    }
});
