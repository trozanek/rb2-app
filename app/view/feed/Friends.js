Ext.define('RB2.view.feed.Friends', {
	extend: 'Ext.List',
	xtype: 'feedfriends',
	requires: ['RB2.store.Feed'],
	config: {
		store: {
			xtype: 'feedstore',
			filters: [
				{ 
					property: 'scopeUsr',
					value: true
				}
			]
		},
		loadingText: null,	
		emptyText: 'Brak aktualności',
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		cls: 'partners-list feed',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile"><img src="'+RB2.Config.getEnv().url+'{avatar}"/></div><div class="desc"><h2>{head}</h2><h3>{content}</h3></div></tpl>')
	}
});