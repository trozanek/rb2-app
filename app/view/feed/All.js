Ext.define('RB2.view.feed.All', {
	extend: 'Ext.List',
	xtype: 'feedall',
	requires: ['RB2.store.Feed'],
	config: {
		store: 'Feed',
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		emptyText: RB2.util.i18n.translate('NoFeed'),
		loadingText: null,
		cls: 'partners-list feed',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile"><img src="'+RB2.Config.getEnv().url+'{avatar}"/></div><div class="desc"><h2>{head}</h2><h3>{content}</h3></div></tpl>')
	}
});