Ext.define('RB2.view.partners.PartnersSearch', {
	extend: 'Ext.Container',
	xtype: 'partnerssearch',
	requires: [
		'RB2.view.partners.PartnersSearchForm',
		'RB2.view.partners.PartnersSearchResults'
	],
	config: {
		layout: 'fit',
		items: [
			{
				xtype: 'partnerssearchform',
				docked: 'top'
			},
			{
				xtype: 'partnerssearchresults'
			}
		]
	}
})