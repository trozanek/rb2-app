Ext.define('RB2.view.partners.YourPartners', {
	extend: 'Ext.List',
	xtype: 'yourpartners',
	config: {
		store: {
			xtype: 'partnersstore',
			filters: [
				{ 
					property: 'fav',
					value: true
				}
			]
		},	
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		emptyText: RB2.util.i18n.translate('emptyFavs'),
		cls: 'partners-list',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile"><img src="{logo}"/></div><div class="desc"><h2>{name}</h2><h3>{[this.translate("Znizka")]} {discount} {discount_sign}</h3></div><div style="position: absolute; right: 2.6em; bottom: 0; background: #CCC; color: #FFF; padding: .3em; font-size: .6em;">{[this.getDistance(values.lokale[0])]} KM</tpl>',
			{
				getDistance: function(values) {
					var distance = RB2.util.MapsTools.distanceToUsr(values.lat, values.lon)
					return distance;
				},
				translate: function(value) {
					return RB2.util.i18n.translate(value);
				}
			}),
		listeners: {
			show: function() {
				

			},
			itemtap: function(item, index, target, record, e, eOpts) {
				this.up('main').down('partner').setData(record.data);
				console.log(item);
				RB2.app.redirectTo('partner');
				this.up('main').down('#navedcontainer').setActiveItem('partner');
			}
		}
	}
});