Ext.define('RB2.view.partners.Categories', {
	extend: 'Ext.List',
	xtype: 'partnerscategories',
	config: {
		store: 'Categories',
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		cls: 'cat-list',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile cat-tile cat-{id}"><span>{[this.translateName(values)]}</span></div></tpl>',
			{	
				translateName: function(values) {	
					return RB2.util.i18n.translate(values.name);
				}
			}
		),
		emptyText: '<div>'+RB2.util.i18n.translate('emptyCategories')+'</div>',
		itemHeight: '4.8em',
		listeners: {
			itemtap: function(item, index, target, record, e, eOpts) {
				var wrapper = this.up('partnerscategorieswrapper');

				Ext.getStore('Subcategories').setData(record.data.subcategories);
				
				wrapper.setActiveItem({xtype: 'partnerssubcategories'});
				wrapper.down('button[action=backtocategories]').show();
				wrapper.down('button[action=backtocategories]').setText('<div class="x-list-item"><div class="desc"><h2>'+record.data.name+'</h2></div></div>');
				
			},
			painted: function() {
				this.setItemHeight();
				console.log('painted');
			},
			storeLoaded: function() {
				var self=this;
				setTimeout(function(){
					self.setItemHeight();	
				}, 500);
				
				console.warn('storeLoaded');	
			},
			initialize: function() {
				var self=this;
				Ext.Viewport.on('orientationchange', function(){
					self.setItemHeight();
				})
				this.setItemHeight();
			},
			updatedata: function() {
				this.setItemHeight();
				console.log('categories updated');

			},
			chosen: function() {
				console.log('chosen');
			}
		}

	},
	setItemHeight: function() {
		var items = Ext.query('.cat-list .x-list-item'),
			itemWidth = items.clientWidth;
		
		for (i=0;i<items.length;i++) {
			itemWidth = items[i].clientWidth;
			items[i].style.height = itemWidth+'px';
		}
	}
	
});