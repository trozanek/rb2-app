Ext.define('RB2.view.partners.PartnersSearchForm', {
	extend: 'Ext.form.Panel',
	xtype: 'partnerssearchform',
	config: {
		scrollable: null,
		height: '3em',
		items: [
			{
				xtype: 'fieldset',
				items: [
					{
						xtype: 'textfield',
						action: 'search'
					}
				]
			}
		],
		listeners: {
			
		}
	}
});