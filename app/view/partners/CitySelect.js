Ext.define('RB2.view.partners.CitySelect', {
	extend: 'Ext.Picker',
	xtype: 'cityselect',
	config: {
		doneButton: RB2.util.i18n.translate('Change'),
        cancelButton: RB2.util.i18n.translate('Cancel'),
        slots: [{
            name: 'city',
            title: RB2.util.i18n.translate('City'),
            data: [{
	                text: 'Białystok',
	                value: 'Białystok'
	            }, {
	                text: 'Bydgoszcz',
	                value: 'Bydgoszcz'
	            }, {
	                text: 'Chorzów',
	                value: 'Chorzów'
	            }, {
	                text: 'Częstochowa',
	                value: 'Częstochowa'
	            }, {
	                text: 'Dąbrowa Górnicza',
	                value: 'Dąbrowa Górnicza'
	            }, {
	                text: 'Elbląg',
	                value: 'Elbląg'
	            }, {
	                text: 'Gliwice',
	                value: 'Gliwice'
	            }, {
	                text: 'Gorzów Wielkopolski',
	                value: 'Gorzów Wielkopolski'
	            }, {
	                text: 'Inowrocław',
	                value: 'Inowrocław'
	            }, {
	                text: 'Kalisz',
	                value: 'Kalisz'
	            }, {
	                text: 'Katowice',
	                value: 'Katowice'
	            }, {
	                text: 'Kielce',
	                value: 'Kielce'
	            }, {
	                text: 'Kołobrzeg',
	                value: 'Kołobrzeg'
	            }, {
	                text: 'Konin',
	                value: 'Konin'
	            }, {
	                text: 'Koszalin',
	                value: 'Koszalin'
	            }, {
	                text: 'Kraków',
	                value: 'Kraków'
	            }, {
	                text: 'Legnica',
	                value: 'Legnica'
	            }, {
	                text: 'Leszno',
	                value: 'Leszno'
	            }, {
	                text: 'Koszalin',
	                value: 'Koszalin'
	            }, {
	                text: 'Lublin',
	                value: 'Lublin'
	            }, {
	                text: 'Luboń k. Poznania',
	                value: 'Luboń k. Poznania'
	            }, {
	                text: 'Łódź',
	                value: 'Łódź'
	            }, {
	                text: 'Nowy Sącz',
	                value: 'Nowy Sącz'
	            }, {
	                text: 'Olsztyn',
	                value: 'Olsztyn'
	            }, {
	                text: 'Opole',
	                value: 'Opole'
	            }, {
	                text: 'Pabianice',
	                value: 'Pabianice'
	            }, {
	                text: 'Piła',
	                value: 'Piła'
	            }, {
	                text: 'Płock',
	                value: 'Płock'
	            }, {
	                text: 'Poznań',
	                value: 'Poznań'
	            }, {
	                text: 'Pruszcz Gdański',
	                value: 'Pruszcz Gdański'
	            }, {
	                text: 'Radom',
	                value: 'Radom'
	            }, {
	                text: 'Rewa',
	                value: 'Rewa'
	            }, {
	                text: 'Rybnik',
	                value: 'Rybnik'
	            }, {
	                text: 'Rzeszów',
	                value: 'Rzeszów'
	            }, {
	                text: 'Rzgów',
	                value: 'Rzgów'
	            }, {
	                text: 'Słupsk',
	                value: 'Słupsk'
	            }, {
	                text: 'Sosnowiec',
	                value: 'Sosnowiec'
	            }, {
	                text: 'Świnoujście',
	                value: 'Świnoujście'
	            }, {
	                text: 'Szczecin',
	                value: 'Szczecin'
	            }, {
	                text: 'Tarnów',
	                value: 'Tarnów'
	            }, {
	                text: 'Koszalin',
	                value: 'Koszalin'
	            }, {
	                text: 'Toruń',
	                value: 'Toruń'
	            }, {
	            	text: 'Trójmiasto',
	            	value: 'Trójmiasto'
	            }, {
	                text: 'Tychy',
	                value: 'Tychy'
	            }, {
	                text: 'Wałbrzych',
	                value: 'Wałbrzych'
	            }, {
	                text: 'Warszawa',
	                value: 'Warszawa'
	            }, {
	                text: 'Wąwelno',
	                value: 'Wąwelno'
	            }, {
	                text: 'Wrocław',
	                value: 'Wrocław'
	            }, {
	                text: 'Zielona Góra',
	                value: 'Zielona Góra'
	            }
            ]
        }],
        listeners: {
        	change: function(picker, value) {
        		
        	}
        }
	}
})