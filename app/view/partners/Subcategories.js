Ext.define('RB2.view.partners.Subcategories', {
	extend: 'Ext.List',
	xtype: 'partnerssubcategories',
	config: {
		store: 'Subcategories',
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		cls: 'partners-list no-tile',
		emptyText: 'W tej kategorii nie mamy jeszcze żadnych partnerów',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="desc"><h2>{name}</h2></div></tpl>'),
		listeners: {
			itemtap: function(item, index, target, record, e, eOpts) {
				var wrapper = this.up('partnerscategorieswrapper');
				Ext.getStore('Categoriespartners').setData(record.data.partners);
				
				wrapper.setActiveItem({xtype: 'categoriespartners'});
				wrapper.down('button[action=backtosubcategories]').show();
				wrapper.down('button[action=backtosubcategories]').setText('<div class="x-list-item"><div class="desc"><h2>'+record.data.name+'</h2></div></div>');
				wrapper.down('button[action=backtocategories]').hide();
				
			}
		}

	}
	
});