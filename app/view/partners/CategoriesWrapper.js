Ext.define('RB2.view.partners.CategoriesWrapper', {
	extend: 'Ext.Container',
	xtype: 'partnerscategorieswrapper',
	requires: ['RB2.view.partners.Categories', 'RB2.view.partners.Subcategories', 'RB2.view.partners.CategoriesPartners'],
	config: {
		layout: 'card',
		items: [
			{
				xtype: 'titlebar',
				docked: 'top',
				title: RB2.util.i18n.translate('Categories'),
				items: [
					// {
					// 	xtype: 'button',
					// 	text: 'back cat',
					// 	align: 'left',
					// 	action: 'backtocategories',
					// 	hidden: true
					// },
					// {
					// 	xtype: 'button',
					// 	text: 'back sub',
					// 	align: 'left',
					// 	action: 'backtosubcategories',
					// 	hidden: true
					// }
				]
			},
			{
				xtype: 'button',
				cls: 'partners-list no-tile back-button',
				iconCls: 'previous',
				text: '<div class="x-list-item"><div class="desc"><h2>Back</h2></div></div>',
				action: 'backtosubcategories',
				docked: 'top',
				hidden: true
			},
			{
				xtype: 'button',
				cls: 'partners-list no-tile back-button',
				iconCls: 'previous',
				text: '<div class="x-list-item"><div class="desc"><h2>Back</h2></div></div>',
				action: 'backtocategories',
				docked: 'top',
				hidden: true
			},
			{
				xtype: 'partnerscategories'
			},
			{
				xtype: 'partnerssubcategories'
			},
			{
				xtype: 'categoriespartners'
			}
		]

	}
	
});