Ext.define('RB2.view.partners.PartnersNearby', {
	extend: 'Ext.Map',
	xtype: 'partnersnearby',
	
	config: {
		
	    mapOptions:{
	        zoom: 15,
	        disableDefaultUI: true,
	    //--Available Map Options--//
	    	// center : new google.maps.LatLng(window.localStorage.getItem('RB2-last-known-lat'),window.localStorage.getItem('RB2-last-known-lon')),
	        panControl: false,
	        draggable: false,
	        zoomControl: true,
	        mapTypeControl: false,
	        scaleControl: true,
	        streetViewControl: true,
	        overviewMapControl: false
	    },
	    listeners: {
	    	chosen: function() {
	    		console.log('map chosen');
	    	},
	    	centerchange: function(self, map, center, eOpts) {
	    		
	    	},
	    	painted: function() {
	    		// this.centerMap();
	    		console.warn('map painted');
	    	},
	    	maprender: function() {
	    		console.warn('map render');
	    		if (RB2.userLon && RB2.userLat) {
	    			this.setMapCenter({latitude: RB2.userLat, longitude: RB2.userLon});
	    		} else {
	    			this.setMapCenter({latitude: 52.411634, longitude: 16.925812});
	    		}

	    		
	    		var records = Ext.getStore('Partners'),
	    			self = this,
	    			shape = {
	    				coord: [0,0,15],
	    				type: 'circle'
	    			};
	    			
	    		records.each(function(record) {
	    			var lokale = record.data.lokale,
	    				partner = record.data;

	    			if (lokale || lokale!==undefined) {
		    			for ( var i=0; i<lokale.length;i++) {
		    				var shape = {
		    					coord: [0,0, 35,0, 35,35, 0,35],
      							type: 'poly'
		    				};
		    				var marker = new google.maps.Marker({
		    					position: new google.maps.LatLng(lokale[i].lat, lokale[i].lon),
		    					map: Ext.ComponentQuery.query('partnersnearby')[0].getMap(),
		    					icon: new google.maps.MarkerImage(partner.logo, null, null ,null ,new google.maps.Size(35,35)),
		    					shape: shape,
		    					partnerRecord: record

		    				});
		    				google.maps.event.addListener(marker, 'click', function() {
		    					Ext.ComponentQuery.query('partner')[0].setData(record.data);
								RB2.app.redirectTo('partner');
								Ext.ComponentQuery.query('main')[0].down('#navedcontainer').setActiveItem('partner');
		    				})
		    				// console.log(marker);
		    				// console.log(lokale[i]);
		    				// console.log(partner);
		    			}
		    		}
	    		});
	    		self.centerMap();

	    		 
	    	}
	    }
	},
	centerMap: function() {
		var self = this;
		if (Ext.ComponentQuery.query('partnersnearby')[0].getMap() !== null) {
			self.setMapCenter({ latitude: RB2.userLat, longitude: RB2.userLon});
		}
	}
	
});