Ext.define('RB2.view.partners.PartnersCity', {
	extend: 'Ext.List',
	xtype: 'partnerscity',
	requires: ['RB2.view.partners.CitySelect'],
	config: {
		store: {
			xtype: 'citystore'
		},
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		cls: 'partners-list',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile"><img src="{logo}"/></div><div class="desc"><h2>{name}</h2><h3>{[this.translate("Znizka")]} {discount} {discount_sign}</h3></div><div style="position: absolute; right: 2.6em; bottom: 0; background: #CCC; color: #FFF; padding: .3em; font-size: .6em;">{[this.getDistance(values.lokale[0])]} KM</tpl>',
			{
				getDistance: function(values) {
					var distance = RB2.util.MapsTools.distanceToUsr(values.lat, values.lon)
					return distance;
				},
				translate: function(value) {
					return RB2.util.i18n.translate(value);
				}
			}
			),
		itemHeight: 'auto',
		emptyText: RB2.util.i18n.translate('emptyCityPartners'),
		listeners: {
			itemtap: function(item, index, target, record, e, eOpts) {
				this.up('main').down('partner').setData(record.data);
				RB2.app.redirectTo('partner');
				this.up('main').down('#navedcontainer').setActiveItem('partner');
			},
			painted: function() {
				this.cityName = RB2.util.CurrentUser.getCity();

				this.filterPartners();
				this.setCityInTitle();
				
			},
			chosen: function() {
				if (this.cityName === undefined || this.cityName === '') {
					Ext.Viewport.add(Ext.create('RB2.view.partners.CitySelect'));
				}
			}
		}
	},
	filterPartners: function() {
		var store = this.getStore(),
			cityName = this.cityName,
			partnerFilter;
		partnerFilter = new Ext.util.Filter({
            filterFn: function(item) {
            	var lokale = item.data.lokale;
            	for (var i=0; i < lokale.length; i++) {
            		if (cityName === 'Trójmiasto') {
						if (lokale[i].cityName === 'Gdańsk' || lokale[i].cityName === 'Gdynia' || lokale[i].cityName === 'Sopot') {
							return true;
						}
					} else if (cityName === 'Łódź') {
						if (lokale[i].cityName === 'Łódź' || lokale[i].cityName === 'Aleksandrów Łódzki' || lokale[i].cityName === 'Pabianice') {
							return true;
						}
					} else {
						if (lokale[i].cityName === cityName) {
	            			return true;
	            		}
					}
            		
            	}
            }
        });
        store.setFilters(partnerFilter);
        store.load();
	},
	setCityInTitle: function() {
		var cityName = this.cityName;
		if (cityName === undefined || cityName === '') {
			this.up().down('#cityTitlebar').setTitle('Wybierz miasto');
		} else {
			this.up().down('#cityTitlebar').setTitle(cityName);
		}
	}
});