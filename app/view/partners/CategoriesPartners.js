Ext.define('RB2.view.partners.CategoriesPartners', {
	extend: 'Ext.List',
	xtype: 'categoriespartners',
	config: {
		store: 'Categoriespartners',
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		grouped: true,
		cls: 'partners-list',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile"><img src="{logo}"/></div><div class="desc"><h2>{name}</h2><h3>Zniżka do {discount} {discount_sign}</h3></div></tpl>'),
		emptyText: '<div style="background: #CCC">Brak wyników</div>',
		itemHeight: function() {
			var width = Ext.componentQuery.query('.tile')[0].width;
			return width;
		},
		listeners: {
			itemtap: function(item, index, target, record, e, eOpts) {

				var partnersStore = Ext.getStore('Partners'),
					selectedId = record.data.id,
					partnerIndex = partnersStore.findExact('id', selectedId),
					partnerRecord = partnersStore.getAt(partnerIndex);
				console.log(selectedId);
				this.up('main').down('partner').setData(partnerRecord.data);
				this.up('main').down('#navedcontainer').setActiveItem('partner');
				RB2.app.redirectTo('partner');
			}
		}

	}
	
});