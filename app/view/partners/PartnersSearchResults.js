Ext.define('RB2.view.partners.PartnersSearchResults', {
	extend: 'Ext.List',
	xtype: 'partnerssearchresults',
	config: {
		store: 'SearchStore',
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		// grouped: true,
		cls: 'partners-list',
		itemTpl: new Ext.XTemplate('<tpl for="."><div class="tile"><img src="{logo}"/></div><div class="desc"><h2>{name}</h2><h3>{[this.translate("Znizka")]} {discount} {discount_sign}</h3></div><div style="position: absolute; right: 2.4em; bottom: 0; background: #CCC; color: #FFF; padding: .3em; font-size: .6em;">{[this.getDistance(values.lokale[0])]} KM</tpl>',
			{
				getDistance: function(values) {
					var distance = RB2.util.MapsTools.distanceToUsr(values.lat, values.lon)
					return distance;
				},
				translate: function(value) {
					return RB2.util.i18n.translate(value);
				}
			}),
		emptyText: RB2.util.i18n.translate('emptySearch'),
		listeners: {
			initialize: function() {
				var store = Ext.getStore('SearchStore');
				store.setFilters([
		            {
		                property: 'name',
		                value: '52f9hdpydf8',
		                anyMatch: true,
		                caseSensitive: false
		            }
		        ]);
		        store.load();
			},
			show: function() {

			},
			itemtap: function(item, index, target, record, e, eOpts) {
				this.up('main').down('partner').setData(record.data);
				RB2.app.redirectTo('partner');
				this.up('main').down('#navedcontainer').setActiveItem('partner');
			}
		}
	}
});