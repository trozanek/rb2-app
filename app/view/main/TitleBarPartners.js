Ext.define('RB2.view.main.TitleBarPartners', {
    extend: 'Ext.TitleBar',
    xtype: 'maintitlebarpartners',
    config: {
		docked: 'top',
		title: RB2.util.i18n.translate('Partners'),
		items: [
			{
				xtype: 'button',
				iconCls: 'rbear-back',
				align: 'left',
				action: 'toggleleftmenu'
			},
			{
				xtype: 'button',
				iconCls: 'compass',
				cls: 'aligned-right',
				iconMask: true,
				align: 'right',
				action: 'showmap',
				hidden: true
			},
			{
				xtype: 'button',
				iconCls: 'refresh',
				cls: 'aligned-right',
				iconMask: true,
				align: 'right',
				style: 'margin-top: 0; margin-right: .7em',
				action: 'refreshpartners'
			},
			{
				xtype: 'button',
				iconCls: 'rbear-back',
				align: 'left',
				action: 'showpartners',
				hidden: true
			},
			{
				xtype: 'button',
				iconCls: 'list',
				cls: 'aligned-right',
				iconMask: true,
				align: 'right',
				action: 'showpartnermenu',
				hidden: true
			}
		]
		
    }
});
