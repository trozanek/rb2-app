Ext.define('RB2.view.main.Navigation', {
	extend: 'Ext.Container',
	xtype: 'mainnavigation',
	
	config: {
		cls: 'nav',
        itemId: 'left-menu',
        layout: 'fit',
        cls: 'left-nav-panel nav-panel',
        items: [
            {
                xtype: 'titlebar',
                docked: 'top',
                title: 'RabaBear'
            }, {
                xtype: 'container',
                scrollable: true,
                cls: 'nav',
                items: [
                	{
                		xtype: 'button',
                		text: RB2.util.i18n.translate('Partners'),
                		action: 'showpartnersmenu'
                	},
                	{
                		xtype: 'button',
                		text: RB2.util.i18n.translate('NewsFeed'),
                		action: 'showfeedmenu'
                	},
                	{
                		xtype: 'button',
                		text: RB2.util.i18n.translate('Transactions'),
                		action: 'showtransactionsmenu'
                	},
                	// {
                	// 	xtype: 'button',
                	// 	text: 'Nagrody',
                	// 	action: 'showrewardsmenu'
                	// },
                    {
                        xtype: 'button',
                        text: RB2.util.i18n.translate('SearchPartners'),
                        action: 'showsearchview'
                    },
                    {
                        xtype: 'button',
                        text: RB2.util.i18n.translate('ScanQR'),
                        action: 'showqrform',
                        style: 'background-image: url(resources/images/qr-bottom.png); background-position: left bottom; background-size: auto 80%; background-repeat: no-repeat;'
                    }
                ]
            }, {
                xtype: 'container',
                docked: 'bottom',
                items: [
                    {
                        xtype: 'container',
                        name: 'loggedUserAvatar',
                        tpl: new Ext.XTemplate('<div class="logged-usr-avatar"><img src="{avatar}"/></div><p class="usr-login">{login}</p>')
                    },
                    {
                        xtype: 'button',
                        cls: 'logout-btn',
                        text: RB2.util.i18n.translate('logOut'),
                        action: 'logmeout'
                    }
                ]
            }
        ],
        listeners: {
            painted: function() {
                this.down('[name=loggedUserAvatar]').setData(Ext.getStore('CurrentUser').getAt(0).data);
            }
        }
	}
});