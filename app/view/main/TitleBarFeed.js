Ext.define('RB2.view.main.TitleBarFeed', {
    extend: 'Ext.TitleBar',
    xtype: 'maintitlebarfeed',
    config: {
		docked: 'top',
		title: RB2.util.i18n.translate('NewsFeed'),
		items: [
			{
				xtype: 'button',
				iconCls: 'rbear-back',
				align: 'left',
				action: 'toggleleftmenu'
			}
		]
		
    }
});
