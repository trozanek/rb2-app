Ext.define('RB2.view.main.TitleBarTrans', {
    extend: 'Ext.TitleBar',
    xtype: 'maintitlebartrans',
    config: {
		docked: 'top',
		title: RB2.util.i18n.translate('Transactions'),
		items: [
			{
				xtype: 'button',
				iconCls: 'rbear-back',
				align: 'left',
				action: 'toggleleftmenu'
			}
		]
		
    }
});
