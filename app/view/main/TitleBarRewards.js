Ext.define('RB2.view.main.TitleBarRewards', {
    extend: 'Ext.TitleBar',
    xtype: 'maintitlebarrewards',
    config: {
		docked: 'top',
		title: 'Nagrody',
		items: [
			{
				xtype: 'button',
				iconCls: 'rbear-back',
				align: 'left',
				action: 'toggleleftmenu'
			}
		]
		
    }
});
