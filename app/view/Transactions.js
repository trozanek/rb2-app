Ext.define('RB2.view.Transactions', {
	extend: 'Ext.Container',
	xtype: 'transactions',
	requires: ['RB2.view.trans.Open', 'RB2.view.trans.Closed', 'RB2.view.trans.Pending', 'RB2.view.trans.Cancelled' ],
	config: {
		layout: 'fit',
		items: [
			{
				xtype: 'maintitlebartrans'
			}, {
				xtype: 'carousel',
				cls: 'carousel',
				items: [
					{
						xtype: 'container',
						layout: 'fit',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: RB2.util.i18n.translate('TransOpened')
							}, {
								xtype: 'transopen'
							}
						]
					},
					{
						xtype: 'container',
						layout: 'fit',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: RB2.util.i18n.translate('TransClosed')
							}, {
								xtype: 'transclosed'
							}
						]
						
					}, {
						xtype: 'container',
						layout: 'fit',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: RB2.util.i18n.translate('TransPending')
							}, {
								xtype: 'transpending'
							}
						]
					}, {
						xtype: 'container',
						layout: 'fit',
						items: [
							{
								xtype: 'titlebar',
								docked: 'top',
								title: RB2.util.i18n.translate('TransCancelled')
							}, {
								xtype: 'transcancelled'
							}
						]
					}
				]
			}

		],
		listeners: {
			initialize: function() {
				
			},
			show: function() {
				
			}
		}
	}
});