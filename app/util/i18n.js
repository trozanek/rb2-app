Ext.define('RB2.util.i18n', {
	singleton: true,

	config: {
		defaultLanguage : 'pl',
		translations: {
			'pl' : {
				'email' : 'Adres E-mail',
				'password' : 'Hasło',
				'passwordConfirm': 'Potwierdź hasło',
				'Nick': 'Nick',
				'logIn' : 'Zaloguj',
				'register' : 'Zarejestruj',
				'BackToLogin': 'Powrót do logowania',
				// Kategorie //
				'Gastronomia' : 'Gastronomia',
				'Turystyka' : 'Turystyka',
				'Rozrywka' : 'Rozrywka',
				'Rekreacja' : 'Rekreacja',
				'Sport' : 'Sport',
				'Zdrowie i Uroda': 'Zdrowie i Uroda',
				'Sklepy' : 'Sklepy',
				'Edukacja' : 'Edukacja',
				'Noclegi' : 'Noclegi',
				'Życie nocne': 'Życie nocne',
				'Kultura' : 'Kultura',
				'Motoryzacja' : 'Motoryzacja',
				'Znizka': 'Zniżka',
				'Discount': 'Znizka',
				'emptyFavs' : 'Nie masz ulubionych partnerów. Sprawdź kategorie (przewiń palcem w lewo) lub wyszukaj (przewiń palcem w prawo). Możesz też poszukać partnerów w pobliżu Ciebie (2 razy w lewo), lub wyświetlić wszystkich w Twoim mieście (3 razy w lewo)',
				'longpressMap': 'Przytrzymaj w dowolnym miejscu mapy aby odblokować sterowanie mapą.',
				'longpressMapUnlock': 'Przytrzymaj mapę aby zablokować sterowanie',
				'emptySearch': 'Wyszukaj partnerów. (Wpisz minimum 3 znaki)',
				'emptyCategories': 'Brak kategorii. Upewnij się że jesteś online.',
				'Search' : 'Szukaj',
				'Favorites': 'Ulubieni',
				'Categories': 'Kategorie',
				'Nearby' : 'W pobliżu',
				'City': 'Miasto',
				'Change': 'Zmień',
				'Cancel': 'Anuluj',
				'Partner': 'Partner',
				'Info': 'Info',
				'Discounts' : 'Zniżki',
				'Venues' : 'Lokale',
				'Offer': 'Oferta',
				'Promo': 'Promocje',
				'TransHistory': 'Historia Transakcji',
				'Reviews': 'Recenzje',
				'PartnerRank': 'Ranking Partnera',
				'Rewards': 'Nagrody',
				'YourLevel': 'Twój Poziom',
				'YourDiscount': 'Twoja zniżka',
				'YourAdvance': 'Twój postęp',
				'Redeem': 'Korzystam ze zniżki',
				'pointsExpireAfter': 'Punkty wygasają po',
				'forEvery' : 'Za każde wydane',
				'onePercentMore': 'Twoja zniżka jest 1% wyższa przy następnej wizycie',
				'onNextVisit': 'zniżki przy następnej wizycie',
				'forSpending': 'Za wydanie w sumie',
				'daysM': 'dniach',
				'Max': 'Maksymalnie',
				'forStart': 'Na początek',
				'Close': 'Zamknij',
				'AmountSpent': 'Wydana kwota',
				'QRInstructions': 'Wpisz w powyższe pole kwotę jaką wydałeś u partnera, u którego się w tej chwili znajdujesz. Pokaż wpisaną kwotę pracownikami, następnie wciśnij "Skanuj" i zeskanuj kod podany przez pracownika.',
				'Scan': 'Skanuj',
				'Partners': 'Partnerzy',
				'NewsFeed': 'Aktualności',
				'Transactions': 'Transakcje',
				'SearchPartners': 'Szukaj Partnerów',
				'ScanQR': 'Skanuj QR',
				'logOut': 'Wyloguj',
				'AllFeed': 'Wszyscy',
				'FriendsOnly': 'Tylko znajomi',
				'PartnersOnly': 'Tylko partnerzy',
				'TransOpened': 'Otwarte',
				'TransClosed': 'Zamknięte',
				'TransPending': 'W realizacji',
				'TransCancelled': 'Anulowane',
				'EmptyTrans': 'Brak transakcji',
				'ToPay': 'Do zapłaty',
				'Sync': 'Synchronizowanie',
				'Moment': 'Momencik',
				'PoorConnection': 'Słabe połącznie z internetem',
				'PoorConnectionInfo': 'Nie masz połączenia z internetem lub jest ono wątpliwej jakości. Brak połączenia utrudnia korzystanie z aplikacji.',
				'WrongLoginData': 'Nieprawidłowe dane logowania',
				'FixData': 'Popraw dane logowania i spróbuj ponownie',
				'Error': 'Błąd',
				'ProvideAllData': 'Podaj wszystkie dane',
				'NoLoginOrPass': 'Nie podałeś adresu e-mail lub hasła',
				'emptyCityPartners': 'Brak partnerów w wybranym mieście',
				'ChangeCity': 'Zmień miasto',
				'Congratulations': 'Gratulacje!',
				'RegisteredInfo': 'Twoje konto w RabaBear zostało zarejestrowane. Zostałeś automatycznie zalogowany.',
				'PasswordsDontMatch': 'Hasła nie pasują do siebie',
				'PasswordsDontMatchInfo': 'Podane przez Ciebie hasło oraz potwierdzenie hasła nie pasują do siebie',
				'Nieprawidłowy format adresu e-mail': 'Nieprawidłowy format adresu e-mail',
				'Na ten adres e-mail jest już zarejestrowane konto': 'Posiadamy już konto zarejestrowane na ten adres e-mail',
				'NoFeed': 'Brak aktualności'
			},
			'en' : {
				'email' : 'E-mail address',
				'password' : 'Password',
				'passwordConfirm': 'Confirm Password',
				'Nick': 'Nick',
				'logIn' : 'Sign In',
				'register' : 'Sign Up',
				'BackToLogin': 'Back to login',
				// Kategorie //
				'Gastronomia' : 'Food',
				'Turystyka' : 'Tourism',
				'Rozrywka' : 'Entertainment',
				'Rekreacja' : 'Recreation',
				'Sport' : 'Sport',
				'Zdrowie i Uroda': 'Health & Beauty',
				'Sklepy' : 'Shopping',
				'Edukacja' : 'Education',
				'Noclegi' : 'Accomodation',
				'Życie nocne': 'Nightlife',
				'Kultura' : 'Culture',
				'Motoryzacja' : 'Automotive',
				'Znizka': 'Discount',
				'Discount': 'Discount',
				'emptyFavs' : 'You have no favorite partners. Check the categories (swipe left once) or search (swipe right once). You can also look for partners nearby you (swipe left twice), or display all partners in your city (swipe left 3 times)',
				'longpressMap': 'Long press a map anywhere to unlock map panning',
				'longpressMapUnlock': 'Long press a map to lock map panning',
				'emptySearch': 'Type at least 3 characters to start searching',
				'emptyCategories': 'No Categories. Make sure you&#39;re online',
				'Search' : 'Search',
				'Favorites': 'Favorites',
				'Categories': 'Categories',
				'Nearby' : 'Near You',
				'City': 'City',
				'Change': 'Change',
				'Cancel': 'Cancel',
				'Partner': 'Partner',
				'Info': 'Info',
				'Discounts' : 'Discounts',
				'Venues' : 'Venues',
				'Offer': 'Offer',
				'Promo': 'Promotions',
				'TransHistory': 'Transactions History',
				'Reviews': 'Reviews',
				'PartnerRank': 'Rank',
				'Rewards': 'Rewards',
				'YourLevel': 'Your Level',
				'YourDiscount': 'Your Discount',
				'YourAdvance': 'Your Advance',
				'Redeem': 'Use your discount',
				'pointsExpireAfter': 'Your points expire after',
				'forEvery' : 'For spending every',
				'onePercentMore': 'your discount is 1% higher at next visit',
				'onNextVisit': 'discount on Your next visit',
				'forSpending': 'For spending in total',
				'daysM': 'days',
				'Max': 'Max',
				'forStart': 'For starters',
				'Close': 'Close',
				'AmountSpent': 'Amount Spent',
				'QRInstructions': 'Type the amount you have spent at partner&#39;s. Show an amount you&#39;ve typed to a staff member. Then press "Scan" and scan QR code passed by staff member',
				'Scan': 'Scan',
				'Partners': 'Partners',
				'NewsFeed': 'News Feed',
				'Transactions': 'Transactions',
				'SearchPartners': 'Search Partners',
				'ScanQR': 'Scan QR',
				'logOut': 'Log Out',
				'AllFeed': 'Everyone',
				'FriendsOnly': 'Friends Only',
				'PartnersOnly': 'Partners Only',
				'TransOpened': 'Opened',
				'TransClosed': 'Closed',
				'TransPending': 'Pending',
				'TransCancelled': 'Cancelled',
				'EmptyTrans': 'No Transactions',
				'RemainingTime': 'RemainingTime',
				'YouSpent': 'You&#39;ve spent',
				'YouGot': 'You got',
				'discountD': 'discount',
				'TimeExceeded': 'Time limit exceeded',
				'ToPay': 'To Pay',
				'Sync': 'Syncing',
				'Moment': 'One Moment Please',
				'PoorConnection': 'Poor Internet connections',
				'PoorConnectionInfo': 'You don&#39;t have internet connections or your connections is poor. No connection makes application less usable.',
				'WrongLoginData': 'Login failed',
				'FixData': 'You&#39;ve entered wrong e-mail or password',
				'Error': 'Error',
				'ProvideAllData': 'Enter all data',
				'NoLoginOrPass': 'You didn&#39;t enter your e-mail or password',
				'emptyCityPartners': 'No partners in chosen city',
				'ChangeCity': 'Change city',
				'Congratulations': 'Congratulations!',
				'RegisteredInfo': 'You have your own RabaBear account now. You&#39; automatically signed in',
				'PasswordsDontMatch': 'Passwords don&#39;t match',
				'PasswordsDontMatchInfo': 'Your password and password confirmation don&#39;t match each other',
				'Nieprawidłowy format adresu e-mail': 'Wrong e-mail address format',
				'Na ten adres e-mail jest już zarejestrowane konto': 'We already have an account registered for that e-mail address',
				'NoFeed': 'No Feed',
				'EmptyOffer': 'No positions in offer',
				'EmptyPromos': 'This partner doesn&#39;t have any active promos now',
				'EmptyReviews': 'Nobody has posted a review. Login to rababear.pl to be a first one',
				'EmptyRank': 'Nobody visited this partner yet. Be the first one!'
			}
		}
	},

	contructor: function(config) {
		this.initConfig(config);
		this.callParent([config]);
	},
	translate: function (key) {
		var browserLanguage = (window.navigator.userLanguage || window.navigator.language).substr(0,2);
		
		var language = this.config.translations[browserLanguage] === undefined ? this.config.defaultLanguage : browserLanguage;
    	
    	var translation = "[" + key + "]";
    	if (this.config.translations[language][key] === undefined) {
    		
    		// Key not found in language : tries default one
    		if (this.config.translations[this.config.defaultLanguage][key] !== undefined) {
    			translation = this.config.translations[this.config.defaultLanguage][key];
    		}
    		
    	} else {
    		
    		// Key found
    		translation = this.config.translations[language][key];
    	}
    	
    	// If there is more than one argument : format string
    	if (arguments.length > 1) {
    		
    	    var tokenCount = arguments.length - 2;
            for( var token = 0; token <= tokenCount; token++ )
    	    {
    	    	translation = translation.replace( new RegExp( "\\{" + token + "\\}", "gi" ), arguments[ token + 1 ] );
    	    }
    		
    	}
    	
    	return translation;
	}
})