Ext.define('RB2.util.CurrentUser', {
	requires: ['RB2.util.LoadPartners', 'RB2.util.LoadTrans'],
	statics: {
		isLogged: function(callback) {
			var UserStore = Ext.getStore('CurrentUser'), status;
			UserStore.load(function(records, operation, success) {
				status = UserStore.data.items.length > 0;	
				callback(status);
			});
		},
		isLoggedRedirect: function () {
			
			this.isLogged(function(status){
				if (!status) {
					console.log('Not Logged');
					Ext.ComponentQuery.query('login')[0].show();
				} else {
					console.log('Logged');
					Ext.ComponentQuery.query('login')[0].hide();
					
					RB2.util.LoadPartners.load();
					RB2.util.LoadTrans.load();
			        RB2.util.LoadPartners.checkEmpty();
			        RB2.util.LoadTrans.checkEmpty();
			        RB2.util.LoadCategories.checkEmpty();

			        window.setInterval(function() {
			            RB2.util.LoadTrans.load();
			        },60000);
			        window.setInterval(function() {
			            RB2.util.LoadPartners.checkEmpty();
			            RB2.util.LoadTrans.checkEmpty();
			        },5000);
			        window.setInterval(function() {
			            Ext.getStore('Feed').load();
			        },90000);
			        
			        RB2.util.LoadCategories.load();
				}
			});
		},
		logIn: function() {
			var LoginStore = Ext.getStore('Login'), UserStore = Ext.getStore('CurrentUser'), UserData, record, self = this;
			UserData = LoginStore.getData();
			if (UserData.items[0].data.session_key) {
				record = UserData.items[0];
				record.setDirty();
				record.save;
				
				UserStore.setData(UserData.items[0].data);
				UserStore.sync();
				self.isLoggedRedirect();
			} else {
				LoginStore.removeAll();
				Ext.Msg.alert(RB2.util.i18n.translate('WrongLoginData'), RB2.util.i18n.translate('FixData'));
			}
			
		},
		logOut: function() {
			var UserStore = Ext.getStore('CurrentUser'), LoginStore = Ext.getStore('Login');
			LoginStore.removeAll();
			window.localStorage.clear();
			UserStore.sync();
			this.isLoggedRedirect();
		},
		getId: function() {
			if (localStorage.getItem('RB2-current-usr')) {
				return localStorage.getItem('RB2-current-usr');
			} else {
				return 0;
			}
		},
		getCity: function() {
			if (localStorage.getItem('RB2-usr-city')) {
				return window.localStorage.getItem('RB2-usr-city')
			} else {
				return Ext.getStore('CurrentUser').getData().all[0].data.miastoName;
			}
		}
	}
})