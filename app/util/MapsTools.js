Ext.define('RB2.util.MapsTools', {
	statics: {
		distance: function(p1,p2) {
			  rad = function(x) {return x*Math.PI/180;}

			  var R = 6371; // earth's mean radius in km
			  var dLat  = rad(p2.lat() - p1.lat());
			  var dLong = rad(p2.lng() - p1.lng());

			  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
			          Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) * Math.sin(dLong/2) * Math.sin(dLong/2);
			  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			  var d = R * c;

			  return parseFloat(d.toFixed(3));
		
		},
        distanceToUsr: function(lat,lon) {
            var p1 = new google.maps.LatLng(lat, lon),
            p2;

            if (RB2.userLat && RB2.userLon) {
                p2  = new google.maps.LatLng(RB2.userLat, RB2.userLon);
            } else {
                if (window.localStorage.getItem('RB2-last-known-lat')) {
                    p2 = new google.maps.LatLng(window.localStorage.getItem('RB2-last-known-lat'), window.localStorage.getItem('RB2-last-known-lon'));    
                } else {
                    p2 = new google.maps.LatLng(52.411634, 16.925812);    
                }
                
            }

            return this.distance(p1,p2);
        },
        distanceInSort: function(record) {
            var lat = record.get('lokale')[0].lat,
            lon = record.get('lokale')[0].lon;

            return this.distanceToUsr(lat,lon);
        },
		lowestDistance: function() {

		},
		minMaxDistance: function(locations) {
			var lowest = new Object,
            	highest = new Object,
            	tmp,
            	result = new Object;

            lowest = {
            	lat: 0,
            	lon: 0,
            	distance: Number.POSITIVE_INFINITY
            }
            highest = {
            	lat: 0,
            	lon: 0,
            	distance: Number.NEGATIVE_INFINITY
            }
            for (var i=locations.length-1; i>=0; i--) {
                tmp = locations[i];
                
                if (tmp.distance < lowest.distance) lowest = tmp;
                if (tmp.distance > highest.distance) highest = tmp;
                

            }
            result = {
            	min: lowest,
            	max: highest
            }
            return(result);
		},
        getAddress: function(lat, lng) {
            var geocoder = new google.maps.Geocoder(),
                latlng   = new google.maps.LatLng(lat, lng);

            geocoder.geocode({'latLng': latlng}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                  console.log(results[0].formatted_address);
                } else {
                  console.info("No results found");
                }
              } else {
                console.info("Geocoder failed due to: " + status);
              }
            });
          }
	}
});