Ext.define('RB2.util.LoadCategories', {
	statics: {
		load: function(store, records, successful) {
			var md5, self = this;

			Ext.Ajax.request({
			   url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().categories,
			   method: 'GET',
			   disableCaching: true,
			   success: function(response, opts) {
			   		var localmd5 = window.localStorage.getItem('RB2-categories-md5'),
			   			resultSet = Ext.JSON.decode(response.responseText),
			   			remotemd5 = resultSet.md5,
			   			records = resultSet.result;

			   			// console.log(localmd5, remotemd5);
			   		window.localStorage.setItem('RB2-categories-raw', response.responseText);
			   		if (localmd5) {
			   			if (localmd5 === remotemd5) {
			   				console.info('Categories already synced');
			   			} else {
			   				
				   			self.syncData(records, remotemd5);
			   			}
			   		} else {
			   				self.syncData(records, remotemd5);
			   		
			   		}
			   },
			   failure: function(response, opts) {
			      console.log('server-side failure with status code ' + response.status);
			   }
			});    	
		},
		syncData: function(records, remotemd5) {
			
			console.log('Syncing Categories');
			var CategoriesStore = Ext.getStore('Categories'),
				categoryData;

			CategoriesStore.load();
			for (i=0; i<records.length; i++) {
				var item = Ext.create('RB2.model.Categories', records[i]);
				CategoriesStore.add(item);
			}
			CategoriesStore.sync();


			console.log('Finished syncing categories');
			categoryData = CategoriesStore.getData().all;
			
			window.localStorage.setItem('RB2-categories-md5', remotemd5);
		},
		checkEmpty: function() {

			if (!Ext.getStore('Categories').getData().all.toString()  && window.localStorage.getItem('RB2-categories-md5')) {
				
				var resultSet = Ext.JSON.decode(window.localStorage.getItem('RB2-categories-raw')),
	   			remotemd5 = resultSet.md5,
	   			records = resultSet.result;

	   			console.warn('No Categories Data. RESTORING');

				this.syncData(records, remotemd5);
	   		}
		}
	}
});