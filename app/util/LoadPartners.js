Ext.define('RB2.util.LoadPartners', {
	statics: {
		load: function(store, records, successful) {
			var md5, self = this;

			Ext.Ajax.request({
			   url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().partners,
			   method: 'GET',
			   disableCaching: true,
			   params: {
					'usr': window.localStorage.getItem('RB2-current-usr')
			   },
			   success: function(response, opts) {
			   		var localmd5 = window.localStorage.getItem('RB2-partners-md5'),
			   			resultSet = Ext.JSON.decode(response.responseText),
			   			remotemd5 = resultSet.md5,
			   			records = resultSet.result;

			   			// console.log(localmd5, remotemd5);
			   		window.localStorage.setItem('RB2-partners-raw', response.responseText);
			   		if (localmd5) {
			   			if (localmd5 === remotemd5) {
			   				console.info('Partners already synced');
			   			} else {
			   				Ext.Viewport.setMasked({xtype: 'loadmask', message: RB2.util.i18n.translate('Sync')});
				   			setTimeout(function(){
				   				self.syncData(records, remotemd5);
				   			},500)
			   			}
			   		} else {
			   			Ext.Viewport.setMasked({xtype: 'loadmask', message: RB2.util.i18n.translate('Sync')});
			   			setTimeout(function(){
			   				self.syncData(records, remotemd5);
			   			},500)
			   		}
			   },
			   failure: function(response, opts) {
			      console.log('server-side failure with status code ' + response.status);
			      Ext.Msg.alert(RB2.util.i18n.translate('PoorConnection'), RB2.util.i18n.translate('PoorConnectionInfo'), Ext.emptyFn);
			   }
			});    	
		},
		syncPrompt: function(records, remotemd5) {
			var self = this;
			Ext.Msg.confirm('Nowe dane','Pojawiły się nowe dane o partnerach. Czy chcesz teraz zsynchronizować? Synchronizacja może zająć od kilku sekund do nawet kilku minut i w tym czasie aplikacja nie będzie dostępna. Aby korzystać z aplikacji nie musisz dokonywać synchronizacji w tym momencie lecz niektóre dane mogą być nieaktulane.', function(btn) {
				if (btn === 'yes') {
					Ext.Viewport.setMasked({xtype: 'loadmask', message: RB2.util.i18n.translate('Sync')});
					setTimeout(function(){
						self.syncData(records, remotemd5);
					},500);
					
				}
			});
		},
		syncData: function(records, remotemd5) {
			
			console.log('Syncing Partners');
			var PartnersStore = Ext.getStore('Partners'),
				partnerData;

			PartnersStore.load();
			for (i=0; i<records.length; i++) {
				var item = Ext.create('RB2.model.Partner', records[i]);
				PartnersStore.add(item);
			}
			PartnersStore.sync();


			console.log('Finished syncing');
			partnerData = PartnersStore.getData().all;
			Ext.ComponentQuery.query('yourpartners')[0].getStore().setData(partnerData);
			
			Ext.ComponentQuery.query('partnerscity')[0].getStore().setData(partnerData);

			Ext.getStore('SearchStore').setData(partnerData);
			
			window.localStorage.setItem('RB2-partners-md5', remotemd5);
			Ext.Viewport.setMasked(false);
		},
		checkEmpty: function() {

			if (!Ext.getStore('Partners').getData().all.toString() && window.localStorage.getItem('RB2-partners-md5')) {
				
				var resultSet = Ext.JSON.decode(window.localStorage.getItem('RB2-partners-raw')),
	   			remotemd5 = resultSet.md5,
	   			records = resultSet.result;

	   			console.warn('No Partners Data. RESTORING');

				this.syncData(records, remotemd5);
	   		}
		}
	}
});