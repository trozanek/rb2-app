Ext.define('RB2.util.LoadTrans', {
	statics: {
		load: function(store, records, successful) {
			var md5, self = this;

			Ext.Ajax.request({
			   url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().trans,
			   method: 'GET',
			   disableCaching: true,
			   params: {
					'usr': window.localStorage.getItem('RB2-current-usr')
			   },
			   success: function(response, opts) {
			   		var localmd5 = window.localStorage.getItem('RB2-trans-md5'),
			   			resultSet = Ext.JSON.decode(response.responseText),
			   			remotemd5 = resultSet.md5,
			   			records = resultSet.result;

			   			// console.log(localmd5, remotemd5);
			   		window.localStorage.setItem('RB2-trans-raw', response.responseText);
			   		if (localmd5) {
			   			if (localmd5 === remotemd5) {
			   				console.info('Transactions already synced');
			   			} else {
			   				console.info('New trans data - Syncing');
			   				self.syncData(records, remotemd5);
			   			}
			   		} else {
			   			self.syncData(records, remotemd5);
			   		}

			   },
			   failure: function(response, opts) {
			      console.log('server-side failure with status code ' + response.status);
			   }
			});    	
		},
		syncPrompt: function(records, remotemd5) {
			var self = this;
			Ext.Msg.confirm('Nowe dane','Pojawiły się nowe dane o partnerach. Czy chcesz teraz zsynchronizować?', function(btn) {
				if (btn === 'yes') {
					Ext.Viewport.setMasked({xtype: 'loadmask', message: 'Synchronizowanie'});
					setTimeout(function(){
						self.syncData(records, remotemd5);
					},500);
					
				}
			});
		},
		syncData: function(records, remotemd5) {
			
			console.log('Syncing Transactions');
			var TransStore = Ext.getStore('Trans'),
				transData;

			TransStore.load();
			
			if (records) {
				for (i=0; i<records.length; i++) {
					var item = Ext.create('RB2.model.Trans', records[i]);
					TransStore.add(item);
				}
				TransStore.sync();


				console.log('Finished syncing transactions');
				transData = TransStore.getData().all;

				Ext.ComponentQuery.query('transopen')[0].getStore().setData(transData);
				Ext.ComponentQuery.query('transclosed')[0].getStore().setData(transData);
				Ext.ComponentQuery.query('transpending')[0].getStore().setData(transData);
				Ext.ComponentQuery.query('transcancelled')[0].getStore().setData(transData);
				Ext.ComponentQuery.query('transcancelled')[0].getStore().setData(transData);
				Ext.ComponentQuery.query('partnertransactions')[0].getStore().setData(transData);

				
				
				window.localStorage.setItem('RB2-trans-md5', remotemd5);
				if (records.length > 0) {
					window.localStorage.setItem('RB2-trans-was', true);
				}
			} else {
				console.log('No remote transactions to sync');
			}
			
		},
		checkEmpty: function() {
			if (!Ext.getStore('Trans').getData().all.toString() && window.localStorage.getItem('RB2-trans-was')) {
				
				var resultSet = Ext.JSON.decode(window.localStorage.getItem('RB2-trans-raw')),
	   			remotemd5 = resultSet.md5,
	   			records = resultSet.result;

	   			console.warn('No Trans Data. RESTORING');

				this.syncData(records, remotemd5);
	   		}
		}
	}
});