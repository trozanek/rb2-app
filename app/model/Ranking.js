Ext.define('RB2.model.Ranking', {
	extend: 'Ext.data.Model',
	config: {
		fields: [
			'usr',
			'place',
			'value',
			'usrName',
			'avatar',
			'partner'
		]
	}
})