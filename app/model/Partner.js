Ext.define('RB2.model.Partner', {
	extend: 'Ext.data.Model',
	config: {
		identifier: {
			type: 'uuid'
		},
		fields: [
			'id',
			'fav',
			'logo',
			'cat1',
			'cat2',
			'lat',
			'lon',
			{name: 'level', type: 'int'},
			{name: 'nextLevel', type: 'int'},
			{name: 'advance', type: 'int'},
			'link',
			'name',
			'desc',
			{name: 'narastanie', type: 'int'},
			{name: 'qr_only', type: 'int'},
			'discount',
			'discount_sign',
			'ocena',
			'transakcje',
			'lokale',
			'rewards',
			'menu_categories',
			'menu_items',
			'opinions',
			'rank',
			'znizki',
			'znizka_start',
			'znizka_max',
			'kwotaWzrostu',
			'nagrody',
			'pointsExpire',
			'currency',
			'searchKey'
			

		]
	}
})