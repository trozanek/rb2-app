Ext.define('RB2.model.Trans', {
	extend: 'Ext.data.Model',
	config: {
		identifier: {
			type: 'uuid'
		},
		fields: [
			'id',
			'partner',
			'partnerNazwa',
			'partnerLogo',
			'lokal',
			'lokalName',
			'lokalUlica',
			'lokalNr',
			'data',
			'kwota',
			'promo',
			'promoName',
			{ name: 'status', type: 'int' },
			'statusName',
			'pin',
			'timestamp',
			'usrZnizka',
			'znizka',
			'znizka_sign',
			'stolik',
			'prac',
			'pracName',
			'remainingTime'
		]
	}
})