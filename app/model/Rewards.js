Ext.define('RB2.model.Rewards', {
	extend: 'Ext.data.Model',
	config: {
		fields: [
			'id',
			'name',
			'level',
			'redeemed'
		]
	}
})