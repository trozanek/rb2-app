Ext.define('RB2.model.Feed', {
	extend: 'Ext.data.Model',
	config: {
		identifier: {
			type: 'uuid'
		},
		fields: [
			'id',
			'head',
			'content',
			'avatar',
			'type',
			'scopeUsr',
			'scopePartner',
			'partner'
		]
	}
})