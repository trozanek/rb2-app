Ext.define('RB2.model.Login', {
	extend: 'Ext.data.Model',
	config: {
		modelId: 'Login',
		fields: [
			'id',
			'login',
			'avatar',
			'session_key',
			'mail',
			'phone'
		]
	}
})