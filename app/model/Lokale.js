Ext.define('RB2.model.Lokale', {
	extend: 'Ext.data.Model',
	config: {
		fields: [
			'id',
			'city',
			'lat',
			'lon',
			'mail',
			'name',
			'ulica',
			'cityName',
			'nr'
		]
	}
})