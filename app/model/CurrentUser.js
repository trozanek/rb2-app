Ext.define('RB2.model.CurrentUser', {
	extend: 'Ext.data.Model',
	config: {
		modelId: 'CurrentUser',
		fields: [
			'id',
			'login',
			'avatar',
			'session_key',
			'mail',
			'phone',
			'miasto',
			'miastoName'
		]
	}
})