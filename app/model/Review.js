Ext.define('RB2.model.Review', {
	extend: 'Ext.data.Model',
	config: {
		fields: [
			'id',
			'date',
			'timestamp',
			'usrId',
			'content',
			'rate',
			'usrName',
			'avatar',
			'type',
			'visit_time',
			{name: 'partner', type: 'int'}
		]
	}
})