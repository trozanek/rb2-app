Ext.define('RB2.model.Categories', {
	extend: 'Ext.data.Model',
	config: {
		modelId: 'Categories',
		fields: [
			'id',
			'name',
			'logo',
			'img',
			'results',
			'subcategories',
			'partners',
			'discount',
			'discount_sign',
			'city'
		]
	}
})