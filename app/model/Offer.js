Ext.define('RB2.model.Offer', {
	extend: 'Ext.data.Model',
	config: {
		fields: [
			'id',
			'name',
			'desc',
			'partner',
			'img',
			'cat',
			'catDescr',
			'no',
			'hasVariants',
			'prices',
			'currency'
		]
	}
})