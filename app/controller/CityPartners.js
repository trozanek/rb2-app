Ext.define('RB2.controller.CityPartners', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
        	'changecity' : 'button[action=changecity]',
        	'cityselect' : 'cityselect',
        	'partnerscity': 'partnerscity'
        },
        control: {
        	'changecity' : {
        		tap: 'showpicker'
        	},
        	'cityselect' : {
        		change: 'filterCities'
        	}
        }
    },
    showpicker: function() {
    	Ext.Viewport.add({xtype: 'cityselect'});
    },
    filterCities: function(picker, value) {
    	console.log('change', value);
    	window.localStorage.setItem('RB2-usr-city', value.city);
    	
    	this.getPartnerscity().cityName = RB2.util.CurrentUser.getCity();
    	this.getPartnerscity().filterPartners();
    	this.getPartnerscity().setCityInTitle();
    }
});