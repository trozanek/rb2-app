Ext.define('RB2.controller.Register', {
	extend: 'Ext.app.Controller',
	requires: 'RB2.util.MD5',
	config: {
		refs: {
			'registerBtn' : 'button[action=goRegister]',
			'registerForm': 'register',
			'loginField' : 'textfield[name=registerlogin]',
			'mailField' : 'textfield[name=registermail]',
			'passField' : 'textfield[name=registerpass]',
			'passVerField' : 'textfield[name=registerpass_ver]',
			'cityField': 'selectfield[name=registermiasto]'
		},
		control: {
			'registerBtn' : {
				tap: 'goRegister'
			}
		}
	},
	goRegister: function() {
		var self = this;
		if (self.getLoginField().getValue() && self.getPassField().getValue() && self.getMailField().getValue()) {
			if (self.getPassField().getValue() === self.getPassVerField().getValue()) {
				Ext.Viewport.setMasked({xtype: 'loadmask', message: RB2.util.i18n.translate('Moment')});
				this.getRegisterForm().submit({
					// type: 'jsonp',
					url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().register,
					method: 'POST',
					params: {
						login: self.getLoginField().getValue(),
						pass: RB2.util.MD5.hash(self.getPassField().getValue()),
						pass_ver: RB2.util.MD5.hash(self.getPassVerField().getValue()),
						mail: self.getMailField().getValue()
					},
					success: function(form, result) {
						Ext.Viewport.setMasked(false);
						Ext.Msg.alert(RB2.util.i18n.translate('Congratulations'), RB2.util.i18n.translate('RegisteredInfo'));
						console.log(result.result[0]);
						Ext.getStore('Login').add([
							{
								avatar: "http://rababear.pl/static/template/images/avatar.gif",
								id: result.result[0].uId,
								lang: "pl",
								login: self.getLoginField().getValue(),
								mail: self.getMailField().getValue(),
								session_key: "234as3rf32asaasdd",
								tel: "",
								ver: "1"
							}
						]);
						RB2.util.CurrentUser.logIn();
						Ext.ComponentQuery.query('login')[0].hide();
						Ext.ComponentQuery.query('register')[0].hide();
							
					},
					failure: function(form, result) {
						Ext.Viewport.setMasked(false);
						if (result.msg === 'nouser') {
							Ext.Msg.alert(RB2.util.i18n.translate('WrongLoginData'), RB2.util.i18n.translate('FixData'));
						} else {
							Ext.Msg.alert(RB2.util.i18n.translate('Error'), RB2.util.i18n.translate(result.msg));
						}

					}
				});
			} else {
				Ext.Msg.alert(RB2.util.i18n.translate('PasswordsDontMatch'), RB2.util.i18n.translate('PasswordsDontMatchInfo'));		
			}
		} else {
			Ext.Msg.alert(RB2.util.i18n.translate('ProvideAllData'), RB2.util.i18n.translate('NoLoginOrPass'));
		}
	},
	logMeOut: function() {
		RB2.util.CurrentUser.logOut();	
		this.getApplication().getController('RB2.controller.Navigation').toggleLeftMenu();
	},
	showRegisterForm: function() {
		Ext.ComponentQuery.query('register')[0].show();
	},
	hideRegisterForm: function() {
		Ext.ComponentQuery.query('register')[0].hide();	
	}
})