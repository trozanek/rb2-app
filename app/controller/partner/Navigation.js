Ext.define('RB2.controller.partner.Navigation', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            'partnerMenuButton' : 'button[cls=partner-nav-button]',
            'navedcontainer' : '#navedcontainer',
            'rightMenu' : '#right-menu',
            'partnerContainer' : 'partner',
            'mapBtn' : 'button[action=showmap]',
            'partnerMap' : 'partnermap',
            'partnerLokaliListItem': 'partnerlokale',
            'mapLockBtn': 'button[action=lockmap]',
            'partnersWrapper' : 'partners',
            'nearbyMap': 'partnersnearby',
            'nearbyinfo': '[name=nearbyinfo]'
        },
        control: {
        	'partnerMenuButton' : {
        		tap: 'showPartnerSection'
        	},
            'mapBtn' : {
                tap: 'showMap'
            },
            'partnerLokaliListItem' : {
                itemtap: 'showLokalMap'
            },
            'mapLockBtn': {
                tap: 'toggleMapLock'
            },
            'nearbyMap': {
                initialize: 'mapInit'
            }
        }
    },
    mapInit: function() {
        var self=this;
        this.getNearbyMap().element.on('longpress', function(){
            self.toggleMapLock();
        });

    },
    toggleRightMenu: function() {
        var navedContainer = this.getNavedcontainer(),
            self = this;
            console.log('Toggle right menu');
        if (navedContainer.openedRight) {
            navedContainer.setStyle('-webkit-transform: translate3d(0px, 0px, 0px); -ms-transform: translate3d(0px, 0px, 0px); transform: translate3d(0px, 0px, 0px);');
            setTimeout(function(){
                navedContainer.openedRight = false;
            },300);
            
        } else {
            navedContainer.setStyle('-webkit-transform: translate3d(-85%, 0px, 0px); -ms-transform: translate3d(-85%, 0px, 0px); transform: translate3d(-85%, 0px, 0px);');
            setTimeout(function(){
                
                navedContainer.openedRight = true;
            },300);
            
        }

        navedContainer.element.on('swipe', function() {
            if (navedContainer.openedRight) {
                console.log('swipe');
                self.toggleRightMenu();
            }
        })
    },

    showPartnerSection: function(btn, evt, eOpts) {
    	var action = parseInt(btn.config.action); 
        var navedContainer = this.getNavedcontainer();
    	this.getPartnerContainer().down('carousel').animateActiveItem(action, {type: 'slide'});
    	this.toggleRightMenu();
        navedContainer.down('partner').setMasked(false);
    },
    showPartnerMainMap: function() {
        var data = this.getPartnerContainer.getData().data;
        this.showMap();
        this.getPartnerMap().addMarkers();
    },
    showMap: function() {
        var partnerWrapper = this.getNavedcontainer().down('#partnerWrapper'),
            partnerMap = this.getPartnerMap(),
            self = this;
        if (partnerWrapper.opened) {
            partnerWrapper.setStyle('-webkit-transform: translate3d(0,0,0); -ms-transform: translate3d(0,0,0); transform: translate3d(0,0,0)');
            partnerMap.setStyle('z-index: 0');
            setTimeout(function(){
                partnerWrapper.opened = false;
                partnerWrapper.setStyle('z-index: 999');
            },300);
        } else {
            partnerWrapper.setStyle('-webkit-transform: translate3d(0,80%,0); -ms-transform: translate3d(0,80%,0); transform: translate3d(0,80%,0);');
            setTimeout(function(){
                partnerMap.setStyle('z-index: 999');    
                partnerWrapper.setStyle('z-index: 0');
                partnerWrapper.opened = true;
            },300);
            
        }

        partnerWrapper.element.on('swipe', function() {
            if (partnerWrapper.opened) {
                self.showMap();
            }
        })
    },
    showLokalMap: function(item, index, e, eOpts) {
        var record = eOpts.data,
            lat = record.lat,
            lon = record.lon;

        this.showMap();
        this.getPartnerMap().setMapCenter({ latitude: lat, longitude: lon})
    },
    toggleMapLock: function() {
        var map = this.getPartnerMap();

        if (map.isLocked) {
            console.log('unlocking');
            this.unlockMap();
            setTimeout(function(){
                map.isLocked = false;
            },100);
            
        } else {
            console.log('locking');
            this.lockMap();
            setTimeout(function(){
                map.isLocked = true;
            },100);
            
        }
    },
    lockMap: function() {
        this.getPartnersWrapper().addCls('disabled-carousel');
        this.getNearbyMap().setMapOptions({
            panControl: true, 
            draggable: true
        })
        this.getMapLockBtn().setIconCls('unlocked');
        if (window.localStorage.getItem('maplocks') < 2) {
            this.getNearbyinfo().setHtml(RB2.util.i18n.translate('longpressMapUnlock'));
            window.localStorage.setItem('maplocks', window.localStorage.getItem('maplocks')+1)
        } else {
            if (this.getNearbyinfo()) {
                this.getNearbyinfo().destroy();
            }
        }
    },
    unlockMap: function() {
        this.getPartnersWrapper().removeCls('disabled-carousel');
        this.getPartnersWrapper().down('carousel').setActiveItem(3);
        this.getNearbyMap().setMapOptions({
            panControl: false, 
            draggable: false
        })
        this.getMapLockBtn().setIconCls('locked');
        if (window.localStorage.getItem('maplocks') < 2) {
            this.getNearbyinfo().setHtml(RB2.util.i18n.translate('longpressMap'));
        } else {
           if (this.getNearbyinfo()) {
                this.getNearbyinfo().destroy();
            }
        }
    }
});