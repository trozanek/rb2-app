Ext.define('RB2.controller.Navigation', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            'leftnavbtn': 'button[action=toggleleftmenu]',
            'leftnav' : '#left-menu',
            'navedcontainer' : '#navedcontainer',
            'showPartnersMenuBtn' : 'button[action=showpartnersmenu]',
            'showPartnersBtn' : 'button[action=showpartners]',
            'showFeedBtn' : 'button[action=showfeedmenu]',
            'showTransactionsBtn' : 'button[action=showtransactionsmenu]',
            'showRewardsBtn' : 'button[action=showrewardsmenu]',
            'mainContainer' : 'main',
            'showPartnerMenu' : 'button[action=showpartnermenu]',
            'rightMenu' : '#right-menu',
            'showQrFormBtn' : 'button[action=showqrform]',
            'showQrFormBtnNoToggle' : 'button[action=showqrformnotoggle]',
            'qrForm': 'checkwithqr',
            'refreshBtn': 'button[action=refreshpartners]',
            'showSearchView': 'button[action=showsearchview]',
            'partnersView': 'partners',
            'redeemBtn': 'button[action=redeem]',
        },
        control: {
        	'leftnavbtn' : {
        		tap: 'toggleLeftMenu'
        	},
            'showPartnersMenuBtn' : {
                tap: 'showPartnersToggle'
            },
            'showPartnersBtn' : {
                tap: 'showPartners'
            },
            'showPartnerMenu' : {
                tap: 'toggleRightMenu'
            },
            'showFeedBtn' : {
                tap: 'showFeedToggle'
            },
            'showTransactionsBtn' : {
                tap: 'showTransToggle'
            },
            'showRewardsBtn' : {
                tap: 'showRewardsToggle'
            },
            'showQrFormBtn' : {
                tap: 'showQRForm'
            },
            'showQrFormBtnNoToggle' : {
                tap: 'showQRFormNoToggle'
            },
            'refreshBtn': {
                tap: 'refreshPartners'
            },
            'showSearchView': {
                tap: 'showSearchView'
            },
            'navedcontainer': {
                initialize: 'setupView'
            },
            'redeemBtn': {
                tap: 'showQRFormNoToggle'
            }
        },
        routes: {
            'partner': 'showPartner',
            'partners': 'redirectToPartners',
            'feed': 'redirectToFeed',
            'trans': 'redirectToTrans',
            'rewards': 'redirectToRewards'
        }
    },
    setupView: function() {
        console.log('swipe');

    },
    toggleLeftMenu: function() {
    	var navedContainer = this.getNavedcontainer(),
            self = this;
        
    	if (navedContainer.opened) {
    		navedContainer.setStyle('-webkit-transform: translate3d(0px, 0px, 0px); transform: -ms-translate3d(0px, 0px, 0px); transform: translate3d(0px, 0px, 0px);')
    		
            setTimeout(function(){
                navedContainer.opened = false;
                navedContainer.down('partners').setMasked(false);
            },300);
    	} else {
    		navedContainer.setStyle('-webkit-transform: translate3d(85%, 0px, 0px); -ms-transform: translate3d(85%, 0px, 0px); transform: translate3d(85%, 0px, 0px);')
        
            setTimeout(function(){
                navedContainer.opened = true;
                navedContainer.down('partners').setMasked(true);
                console.log(navedContainer.element);
                console.log(navedContainer.down('partners').down('mask').element);
                
                navedContainer.element.on('swipe', function() {
                    if (navedContainer.opened) {
                        self.toggleLeftMenu();
                    }
                })

            },300);
    	}
    },
    toggleRightMenu: function() {
        var navedContainer = this.getNavedcontainer(),
            self = this;

        if (navedContainer.openedRight) {
            this.closeRightMenu();
            navedContainer.down('partner').setMasked(false);
            
        } else {
            this.openRightMenu();
            navedContainer.down('partner').setMasked(true);
        }

        navedContainer.element.on('swipe', function() {
            if (navedContainer.openedRight) {
                console.log('swipe');
                self.toggleRightMenu();
            }
        })
    },
    closeRightMenu: function() {
         var navedContainer = this.getNavedcontainer(),
            self = this;

        navedContainer.setStyle('-webkit-transform: translate3d(0px, 0px, 0px); -ms-transform: translate3d(0px, 0px, 0px); transform: translate3d(0px, 0px, 0px);');
            
        setTimeout(function(){
            navedContainer.openedRight = false;
        },300);
    },
    openRightMenu: function() {
        var navedContainer = this.getNavedcontainer(),
            self = this;

        navedContainer.setStyle('-webkit-transform: translate3d(-85%, 0px, 0px); -ms-transform: translate3d(-85%, 0px, 0px); transform: translate3d(-85%, 0px, 0px);');

        setTimeout(function(){
            navedContainer.openedRight = true;
        },300);
    },
    showPartners: function() {
        this.redirectTo('partners');
        this.getRightMenu().hide();
        console.log(this.getPartnersView().down('carousel').getActiveIndex());
        if (this.getPartnersView().down('carousel').getActiveIndex() === 0) {
            this.getPartnersView().down('carousel').setActiveItem(1);
        }
    },
    showPartnersToggle: function() {
        this.showPartners();
        this.toggleLeftMenu();
    },
    showFeed: function() {
        this.redirectTo('feed');
        this.getRightMenu().hide();
    },
    showFeedToggle: function() {
        this.showFeed();
        this.toggleLeftMenu();
    },
    showTrans: function() {
        this.redirectTo('trans');
        this.getRightMenu().hide();
    },
    showSearchView: function() {
        this.redirectTo('partners');
        this.toggleLeftMenu();
        this.getPartnersView().down('carousel').setActiveItem(0);
    },
    showRewards: function() {
        this.redirectTo('rewards');
        this.getRightMenu().hide();
    },
    showTransToggle: function() {
        this.showTrans();
        this.toggleLeftMenu();
    },
    showRewardsToggle: function() {
        this.showRewards();
        this.toggleLeftMenu();
    },
    showPartner: function() {
        this.getNavedcontainer().setActiveItem('partner');
        this.getRightMenu().show();
    },
    redirectToPartners: function() {
        this.getNavedcontainer().setActiveItem('partners');
        this.closeRightMenu();
        this.getRightMenu().hide();
    },
    redirectToFeed: function() {
        this.getNavedcontainer().setActiveItem('feed');
        this.getRightMenu().hide();
    },
    redirectToTrans: function() {
        this.getNavedcontainer().setActiveItem('transactions');
        this.getRightMenu().hide();
    },
    redirectToRewards: function() {
        this.getNavedcontainer().setActiveItem('rewards');
        this.getRightMenu().hide();
    },
    showQRForm: function() {
        this.getQrForm().show();
        this.toggleLeftMenu();
    },
    showQRFormNoToggle: function() {
        this.getQrForm().show();
    },
    refreshPartners: function() {
        console.log('Refreshing partners');
        RB2.util.LoadPartners.load();
        RB2.util.LoadCategories.load();
        RB2.util.LoadTrans.load();
    }

});