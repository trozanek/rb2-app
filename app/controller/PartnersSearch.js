Ext.define('RB2.controller.PartnersSearch', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
			searchField: '[action=search]',
			searchResults: 'partnerssearchresults'
		},
        control: {
        	searchField: {
        		keyup: 'filterSearch',
        		change: 'filterSearch'
        	}
        }
    },
    filterSearch: function() {
    	var store = Ext.getStore('SearchStore'),
            field = this.getSearchField().getValue().toLowerCase();
            this.getSearchResults().setMasked({xtype: 'loadmask', message: 'Momencik'});
        if (field == '') {
            store.setFilters([
                {
                    property: 'name',
                    value: 'asdf98034rpyh0df8',
                    anyMatch: true,
                    caseSensitive: false
                }
            ]);
        } else {
            if (field.length > 2) {
                // store.setFilters([
                //     {
                //         property: 'name',
                //         value: this.getSearchField().getValue(),
                //         anyMatch: true,
                //         caseSensitive: false
                //     }, {
                //         property: 'desc',
                //         value: this.getSearchField().getValue(),
                //         anyMatch: true,
                //         caseSensitive: false
                //     }
                // ]);
                
                var partnerFilter = new Ext.util.Filter({
                    filterFn: function(item) {
                        if (item.data.searchKey.toLowerCase().indexOf(field) > -1 ) {
                            
                            return true;
                        }
                    }
                })
                store.setFilters(partnerFilter);
                // this.getSearchResults().setMasked(false);
            } else {
                store.setFilters([
                    {
                        property: 'name',
                        value: 'asdf98034rpyh0df8',
                        anyMatch: true,
                        caseSensitive: false
                    }
                ]);
            }
        }
        store.load();
    }

});