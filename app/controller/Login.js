Ext.define('RB2.controller.Login', {
	extend: 'Ext.app.Controller',
	requires: 'RB2.util.MD5',
	config: {
		refs: {
			'loginForm' : 'login',
			'loginField' : 'login [name=login]',
			'passwordField' : 'login [name=password]',
			'loginBtn' : 'button[action=logmein]',
			'logoutBtn' : 'button[action=logmeout]',
			'registerBtn' : 'button[action=showregisterform]',
			'hideRegisterBtn' : 'button[action=hideregisterform]'
		},
		control: {
			'loginBtn' : {
				tap: 'logMeIn'
			},
			'logoutBtn' : {
				tap: 'logMeOut'
			},
			'registerBtn' : {
				tap: 'showRegisterForm'
			},
			'hideRegisterBtn' : {
				tap: 'hideRegisterForm'
			}
		}
	},
	logMeIn: function() {
		var self = this;
		if (self.getLoginField().getValue() && self.getPasswordField().getValue()) {
			Ext.Viewport.setMasked({xtype: 'loadmask', message: RB2.util.i18n.translate('Moment')});
			this.getLoginForm().submit({
				// type: 'jsonp',
				url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().user,
				method: 'POST',
				params: {
					login: self.getLoginField().getValue(),
					pass: RB2.util.MD5.hash(self.getPasswordField().getValue())
				},
				success: function(form, result) {
					Ext.Viewport.setMasked(false);
					Ext.getStore('Login').add(result.result);
					
					RB2.util.CurrentUser.logIn();
					
				},
				failure: function(form, result) {
					Ext.Viewport.setMasked(false);
					if (result.msg === 'nouser') {
						Ext.Msg.alert(RB2.util.i18n.translate('WrongLoginData'), RB2.util.i18n.translate('FixData'));
					} else {
						Ext.Msg.alert(RB2.util.i18n.translate('Error'), result.msg);
					}

				}
			});
		} else {
			Ext.Msg.alert(RB2.util.i18n.translate('ProvideAllData'), RB2.util.i18n.translate('NoLoginOrPass'));
		}
	},
	logMeOut: function() {
		RB2.util.CurrentUser.logOut();	
		this.getApplication().getController('RB2.controller.Navigation').toggleLeftMenu();
	},
	showRegisterForm: function() {
		Ext.ComponentQuery.query('register')[0].show();
	},
	hideRegisterForm: function() {
		Ext.ComponentQuery.query('register')[0].hide();	
	}
})