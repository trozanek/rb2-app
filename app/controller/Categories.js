Ext.define('RB2.controller.Categories', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
        	'categorieswrapper' : 'partnerscategorieswrapper',
            'backtocategories': 'button[action=backtocategories]',
            'backtosubcategories': 'button[action=backtosubcategories]'
            
        },
        control: {
        	'backtocategories' : {
        		tap: 'backToCategories'
        	},
        	'backtosubcategories' : {
        		tap: 'backToSubCategories'
        	}
        }
    },

    backToCategories: function() {
    	this.getCategorieswrapper().setActiveItem('partnerscategories');
    	this.getCategorieswrapper().down('button[action=backtocategories]').hide(); 	
    },
    backToSubCategories: function() {
    	this.getCategorieswrapper().setActiveItem('partnerssubcategories');
    	this.getCategorieswrapper().down('button[action=backtosubcategories]').hide();
    	this.getCategorieswrapper().down('button[action=backtocategories]').show();
    }
});