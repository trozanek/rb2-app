Ext.define('RB2.controller.QRScanner', {
	extend: 'Ext.app.Controller',

	config: {
		refs: {
			qrForm: 'checkwithqr',
			formCloseBtn : 'button[action=closeform]',
			scannerButton: 'button[action=openscanner]',
			formAmtField : '[name=amount]',
			formAmtConfirm: '#amountConfirmBox',
			qRConfirm: 'qrconfirm',
			confirmCloseBtn: 'button[action=closeconfirm]'
		},
		control: {
			formCloseBtn : {
				tap: 'closeForm'
			},
			scannerButton: {
				tap: 'openScanner'
			},
			formAmtField: {
				keyup: 'showAmt',
        		change: 'showAmt'
			},
			confirmCloseBtn: {
				tap: 'closeQRConfirm'
			}

		},
		routes: {
			'qrconfirm/:id/:ts': 'showQRConfirm'
		}
	},
	closeForm: function() {
		this.getQrForm().hide();
	},
	closeQRConfirm: function() {
		this.getQRConfirm().hide();		
	},
	showAmt: function() {
		if (this.getFormAmtField().getValue()) {
			this.getFormAmtConfirm().setHtml(this.getFormAmtField().getValue() +' PLN');
		} else {
			this.getFormAmtConfirm().setHtml('0 PLN');
		}
		this.getFormAmtConfirm().addCls('info-box');
	},
	showQRConfirm: function(id,ts) {
		this.getQRConfirm().show();
		this.getQRConfirm().fireEvent('codeScanned', id, this.getFormAmtField().getValue(), ts);
		this.getQrForm().hide();
	},
	openScanner: function() {
		// this.redirectTo('rb2scan://scanIt');
		// window.location.href='rb2scan://scanIt';
		// Android.ScanIt('scan');
		this.redirectTo('qrconfirm/9b0ae88b4545f335a4d650587848c5d1/'+new Date().getTime());
	}
})