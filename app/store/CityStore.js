Ext.define('RB2.store.CityStore', {
	extend: 'Ext.data.Store',
	xtype: 'citystore',
	config: {
		model: 'RB2.model.Partner',
		autoLoad: true,
		sorters: [
			{
				sorterFn: function(record1, record2) {

					var dist1 = RB2.util.MapsTools.distanceToUsr(record1.data.lokale[0].lat, record1.data.lokale[0].lon);
					var dist2 = RB2.util.MapsTools.distanceToUsr(record2.data.lokale[0].lat, record2.data.lokale[0].lon);
					return dist1 > dist2 ? 1 : (dist1 === dist2 ? 0 : -1);
				},
				direction: 'ASC'
			}
		],
		listeners: {
			load: function(){
				console.log('City Store Loaded');
				
			}
		}
	}
});