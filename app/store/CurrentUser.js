Ext.define('RB2.store.CurrentUser', {
	extend: 'Ext.data.Store',
	config: {
		model: 'RB2.model.CurrentUser',
		autoLoad: true,
		autoSync: true,
		proxy: {
			type: 'localstorage',
			id: 'RB2-current-usr'
		},
		writer: {
			type: 'json'
		},
		listeners: {
			
		}
	}
})