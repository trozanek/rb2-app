Ext.define('RB2.store.Reviews', {
	extend: 'Ext.data.Store',
	xtype: 'reviewsstore',
	config: {
		model: 'RB2.model.Review',
		autoLoad: true,
		proxy: {
			type: 'ajax',
			url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().reviews,
			reader: {
				type: 'json',
				rootProperty: 'result'
			}
		}
	}
});