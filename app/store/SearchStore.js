Ext.define('RB2.store.SearchStore', {
	extend: 'Ext.data.Store',
	xtype: 'searchstore',
	config: {
		model: 'RB2.model.Partner',
		autoLoad: true,
		// grouper: {
		// 	groupFn: function(record) {
		// 		if (record.get('lokale')[0].cityName === 'Gdańsk' || record.get('lokale')[0].cityName === 'Gdynia' || record.get('lokale')[0].cityName === 'Sopot') {
		// 			return 'Trójmiasto'
		// 		} else if (record.get('lokale')[0].cityName === 'Łódź' || record.get('lokale')[0].cityName === 'Aleksandrów Łódzki' || record.get('lokale')[0].cityName === 'Pabianice') {
		// 			return 'Łódź'	
		// 		} else {
		// 			return record.get('lokale')[0].cityName;
		// 		}
				
		// 	}
		// },
		sorters: [
			{
				sorterFn: function(record1, record2) {

					var dist1 = RB2.util.MapsTools.distanceToUsr(record1.data.lokale[0].lat, record1.data.lokale[0].lon);
					var dist2 = RB2.util.MapsTools.distanceToUsr(record2.data.lokale[0].lat, record2.data.lokale[0].lon);
					console.log(dist1, dist2, dist1 > dist2 ? 1 : (dist1 === dist2 ? 0 : -1));
					return dist1 > dist2 ? 1 : (dist1 === dist2 ? 0 : -1);
				},
				direction: 'ASC'
			}
		],
		listeners: {
			load: function(){
				console.log('Search Store Loaded');
				
			}
		}
	}
});