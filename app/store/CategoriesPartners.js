Ext.define('RB2.store.CategoriesPartners', {
	extend: 'Ext.data.Store',
	xtype: 'categoriespartnersstore',
	config: {
		model: 'RB2.model.Categories',
		storeId: 'Categoriespartners',
		autoLoad: true,
		grouper: {
			groupFn: function(record) {
				if (record.get('city') === 'Gdańsk' || record.get('city') === 'Gdynia' || record.get('city') === 'Sopot') {
					return 'Trójmiasto'
				} else if (record.get('city') === 'Łódź' || record.get('city') === 'Aleksandrów Łódzki' || record.get('city') === 'Pabianice') {
					return 'Łódź'	
				} else {
					return record.get('city');
				}
			}
		},
		proxy: {
			type: 'ajax',
			url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().categories,
			reader: {
				type: 'json',
				rootProperty: 'result'
			}
		}
	}
});