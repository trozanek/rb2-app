Ext.define('RB2.store.Lokale', {
	extend: 'Ext.data.Store',
	xtype: 'lokalestore',
	config: {
		model: 'RB2.model.Lokale',
		storeId: 'Lokale',
		autoLoad: true
	}
});