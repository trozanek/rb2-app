Ext.define('RB2.store.Offer', {
	extend: 'Ext.data.Store',
	xtype: 'offerstore',
	config: {
		model: 'RB2.model.Offer',
		storeId: 'Offer',
		autoDestroy: true,
		autoLoad: false,
		grouper: {
            groupFn: function(record) {
                return record.get('cat');
            },
            sortProperty: 'cat'
        },
		proxy: {
			type: 'ajax',
			url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().menu,
			method: 'GET',
			reader: {
				type: 'json',
				rootProperty: 'result',
				idProperty: 'id',
				messageProperty: 'md5',
				totalProperty: 'total'
			}
		},
		listeners: {
			load: function() {
				
			}
		}
	}
});