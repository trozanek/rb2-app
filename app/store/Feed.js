Ext.define('RB2.store.Feed', {
	extend: 'Ext.data.Store',
	xtype: 'feedstore',
	config: {
		model: 'RB2.model.Feed',
		autoLoad: true,
		proxy: {
			type: 'ajax',
			url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().feed,
			method: 'GET',
			extraParams: {
				'usr': window.localStorage.getItem('RB2-current-usr')
			},
			reader: {
				type: 'json',
				rootProperty: 'wall',
				idProperty: 'id',
				messageProperty: 'md5',
				totalProperty: 'total'
			}
		},
		listeners: {
			load: function() {
				console.log('Refreshing Feed');
			}
		}
	}
});