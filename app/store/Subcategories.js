Ext.define('RB2.store.Subcategories', {
	extend: 'Ext.data.Store',
	xtype: 'subcategoriesstore',
	config: {
		model: 'RB2.model.Categories',
		storeId: 'Subcategories',
		autoLoad: true,
		proxy: {
			type: 'ajax',
			url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().categories,
			reader: {
				type: 'json',
				rootProperty: 'result'
			}
		}
	}
});