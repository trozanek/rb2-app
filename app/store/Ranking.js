Ext.define('RB2.store.Ranking', {
	extend: 'Ext.data.Store',
	xtype: 'rankingstore',
	config: {
		model: 'RB2.model.Ranking',
		storeId: 'Ranking',
		autoDestroy: true,
		autoLoad: false,
		proxy: {
			type: 'ajax',
			url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().ranking,
			method: 'GET',
			reader: {
				type: 'json',
				rootProperty: 'result',
				idProperty: 'id',
				messageProperty: 'md5',
				totalProperty: 'total'
			}
		},
		listeners: {
			load: function() {
				
			}
		}
	}
});