Ext.define('RB2.store.Categories', {
	extend: 'Ext.data.Store',
	xtype: 'categoriesstore',
	config: {
		model: 'RB2.model.Categories',
		autoLoad: true,
		autoSync: true,
		proxy: {
			type: 'localstorage',
			id: 'RB2-category'
		},
		writer: {
			type: 'json'
		},
		listeners: {
			load: function(){
				if (Ext.ComponentQuery.query('partnerscategories')[0]) {
					Ext.ComponentQuery.query('partnerscategories')[0].fireEvent('storeLoaded');
				}
				
			}
		}
	}
});