Ext.define('RB2.store.Rewards', {
	extend: 'Ext.data.Store',
	xtype: 'rewardsstore',
	config: {
		model: 'RB2.model.Rewards',
		storeId: 'Rewards',
		autoLoad: true
	}
});