Ext.define('RB2.store.Partners', {
	extend: 'Ext.data.Store',
	xtype: 'partnersstore',
	config: {
		model: 'RB2.model.Partner',
		autoLoad: true,
		autoSync: true,
		proxy: {
			type: 'localstorage',
			id: 'RB2-partner'
		},
		writer: {
			type: 'json'
		},
		sorters: [
			{
				sorterFn: function(record1, record2) {

					var dist1 = RB2.util.MapsTools.distanceToUsr(record1.data.lokale[0].lat, record1.data.lokale[0].lon);
					var dist2 = RB2.util.MapsTools.distanceToUsr(record2.data.lokale[0].lat, record2.data.lokale[0].lon);
					return dist1 > dist2 ? 1 : (dist1 === dist2 ? 0 : -1);
				},
				direction: 'ASC'
			}
		],
		listeners: {
			load: function(){
				console.log('Local Partners Data Loaded');
				var partnerData = this.getData().all;
				if (Ext.getStore('SearchStore')) {
					Ext.getStore('SearchStore').setData(partnerData);
				}
				if (Ext.getStore('CityStore')) {
					Ext.getStore('CityStore').setData(partnerData);
				}
				
			}
		}
	}
});