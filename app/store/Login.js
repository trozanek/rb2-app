Ext.define('RB2.store.Login', {
	extend: 'Ext.data.Store',
	config: {
		model: 'RB2.model.CurrentUser',
		autoLoad: false,
		proxy: {
			type: 'ajax',
			url: RB2.Config.getEnv().url + RB2.Config.getServicesUrl().user,
			reader: {
				type: 'json',
				rootProperty: 'result',
				idProperty: 'id'
			}
		}
	}
})