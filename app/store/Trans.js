Ext.define('RB2.store.Trans', {
	extend: 'Ext.data.Store',
	xtype: 'transstore',
	config: {
		model: 'RB2.model.Trans',
		autoLoad: true,
		autoSync: true,
		proxy: {
			type: 'localstorage',
			id: 'RB2-trans'
		},
		writer: {
			type: 'json'
		},
		listeners: {
			load: function(){
				
			}
		}
	}
});