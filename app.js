/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides




Ext.application({
    name: 'RB2',
    
    requires: [
        'Ext.MessageBox',
        'RB2.Config',
        'RB2.util.CurrentUser',
        'RB2.util.MapsTools',
        'RB2.util.LoadCategories',
        'RB2.util.i18n'
    ],

    eventPublishers: {
        touchGesture: {
            recognizers: {
                swipe: {
                    //this will include both vertical and horizontal swipe recognisers
                    xclass: 'Ext.event.recognizer.Swipe'
                }
            }
        }
    },
    models:[
        'Categories',
        'Partner',
        'CurrentUser',
        'Login',
        'Lokale',
        'Rewards',
        'Offer',
        'Feed',
        'Trans',
        'Review',
        'Ranking'
    ],

    views: [
        'Main',
        'Partners',
        'partners.CategoriesWrapper',
        'Feed',
        'Transactions',
        'Rewards',
        'Partner',
        'Login',
        'Register',
        'main.TitleBarPartners',
        'main.TitleBarFeed',
        'main.TitleBarTrans',
        'main.TitleBarRewards',
        'main.Navigation',
        'partner.Navigation',
        'feed.All',
        'feed.Friends',
        'feed.Partners',
        'checkWithQR',
        'QRConfirm'

    ],

    stores: [
        'CurrentUser',
        'Partners',
        'Lokale',
        'Categories',
        'Subcategories',
        'CategoriesPartners',
        'Login',
        'Rewards',
        'Offer',
        'Feed',
        'Trans',
        'Reviews',
        'Ranking',
        'SearchStore',
        'CityStore'
    ],

    controllers: [
        'Navigation',
        'Categories',
        'partner.Navigation',
        'Login',
        'PartnersSearch',
        'Checkin',
        'QRScanner',
        'CityPartners',
        'Register'
    ],

    launch: function() {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        // Initialize the main view
        Ext.Viewport.add(Ext.create('RB2.view.Main'));
        var userLocation = Ext.create('Ext.util.Geolocation', {
                    autoUpdate: true,
                    frequency: 5000,
                    listeners: {
                        locationupdate: function(geo) {
                            RB2.userLat = geo.getLatitude();
                            console.log(RB2.userLat);
                            RB2.userLon = geo.getLongitude();
                            window.localStorage.setItem('RB2-last-known-lat', RB2.userLat);
                            window.localStorage.setItem('RB2-last-known-lon', RB2.userLon)
                        },
                        locationerror: function(geo, bTimeout, bPermissionDenied, bLocationUnavailable, message) {
                            if(bTimeout){
                                console.log('Timeout occurred.');
                            } else {
                                console.log('Error occurred.');
                            }
                        },
                        initialize: function() {
                            console.log('geo initialized');
                        }
                    }
                });
                userLocation.updateLocation();

        
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
